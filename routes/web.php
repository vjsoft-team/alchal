<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', function($value='')
{
  return view('welcome');
});
Route::get('/user/logout', 'Auth\LoginController@UserLogout')->name('user.logout');


Route::prefix('disc')->group(function(){
  Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login','Auth\AdminLoginController@Login')->name('admin.login.submit');
  Route::get('/', 'AdminController@index')->name('admin.dashboard');
  Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
  Route::resource('coupons','CouponController');
  Route::resource('shops','ShopController');
  Route::resource('stores','AdminStoreController');
  Route::get('/stores/{id}/edit','AdminStoreController@edit');
  Route::resource('category','CategoryController');
  Route::get('/category/{id}/edit','CategoryController@edit');
  Route::resource('ads','AdsController');
});
Route::prefix('validation')->group(function(){
  Route::get('/', 'StoreController@index')->name('validation.dashboard');
  Route::resource('/coupons','StoreCouponController');
  Route::post('/couponValidate', 'StoreController@couponValidate')->name('validation.validate');
  Route::get('/login','Auth\StoreLoginController@showLoginForm')->name('validation.login');
  Route::post('/login','Auth\StoreLoginController@Login')->name('validation.login.submit');
  Route::post('/logout', 'Auth\StoreLoginController@storeLogout')->name('validation.logout');
});

Route::get('/coupons','FrontendController@lists');
Route::get('/contact-us','FrontendController@contactus');
Route::post('/send','ContactusController@send')->name('send');
Route::get('/thank','ContactusController@thank');
Route::get('/stores/{search}','FrontendController@searchStores');
Route::get('/category/{search}','FrontendController@categorysearch');
Route::get('/stores','FrontendController@AllShops');

Route::get('/terms-and-conditions','FrontendController@terms');
Route::get('/privacy-policy','FrontendController@Privacy');


// Verification
Route::get('verifymobile/{mobile}', 'MobileVerificationController@index');
Route::get('verifymobileemail/{email}', 'MobileVerificationController@verifymobileemail');
Route::post('verifymobile', 'MobileVerificationController@resend')->name('mobile.resend');
Route::post('verify', 'MobileVerificationController@store')->name('mobile.verify');

// Facebook Socialite
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('facebookregister', 'Auth\SociliteController@savefacebookform');
Route::post('facebookregister', 'Auth\SociliteController@savefacebook')->name('facebook.register');

// Favourite Stores
Route::resource('/favouritestores', 'FavouritestoreController');

// My Coupouns
Route::resource('/my-coupoun', 'MyCoupounController');


// profile
Route::resource('/profile', 'ProfileController');
Route::post('/profile/update', 'ProfileController@update');

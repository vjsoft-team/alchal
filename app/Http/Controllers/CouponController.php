<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth:admin');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list = Coupon::all();
        return view('coupons.create')->with('list',$list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validate($request,[
          'store' => 'required',
          'category' => 'required',
          'coupon' => 'required',
          'discount' => 'required',
          'title' => 'required',
          'description' => 'required',
          'valid_from' => 'required',
          'valid_to' => 'required',
        ]);
        $add_coupon = new Coupon();
        $add_coupon->store = $request->store;
        $add_coupon->category = $request->category;
        $add_coupon->coupon = $request->coupon;
        $add_coupon->description = $request->description;
        $add_coupon->title = $request->title;
        $add_coupon->discount = $request->discount;
        $add_coupon->valid_from = $request->valid_from;
        $add_coupon->valid_to = $request->valid_to;
        $add_coupon->save();

        return redirect('/disc/coupons/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function show(Shop $shop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $edit_id = Coupon::find($id);
        return view('coupons.edit')->with('key', $edit_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'store' => 'required',
        'category' => 'required',
        'coupon' => 'required',
        'discount' => 'required',
        'valid_from' => 'required',
        'valid_to' => 'required',
      ]);
      // return $request;
      $edit_coupon = Coupon::find($id);
      $edit_coupon->store = $request->store;
      $edit_coupon->category = $request->category;
      $edit_coupon->coupon = $request->coupon;
      $edit_coupon->discount = $request->discount;
      $edit_coupon->valid_from = $request->valid_from;
      $edit_coupon->valid_to = $request->valid_to;
      $edit_coupon->save();

      return redirect('/disc/coupons/create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $destroy_info = Coupon::find($id);
    $destroy_info->delete();
    return redirect('/disc/coupons/create');
    }
}

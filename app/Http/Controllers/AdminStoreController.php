<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MyCoupoun;
use App\Store;
use Hash;

class AdminStoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // return 'tyuio';
        return view('validation.validation');
    }

    public function create()
    {   $list = Store::all();
        return view('stores.create')->with('list', $list);
    }

    public function store(Request $request)
    {
        //return $request;
      $this->validate($request,[
        'name' => 'required',
        'contact_person_name' => 'required',
        'contact_person_no' => 'required',
        'address' => 'required',
      ]);
      if ($request->hasFile('store_logo')) {

        $file = $request->store_logo;
        $fullPhotoNameWithExt = $file->getClientOriginalName();
        $fileName = pathinfo($fullPhotoNameWithExt, PATHINFO_FILENAME);
        $fileExt = $file->getClientOriginalExtension();
        $photoToSave = $fileName.'_'.time().'.'. $fileExt;
        $path = $file->move('shoplogo', $photoToSave);
      }
      $add_shop = new Store();
      $add_shop->name = $request->name;
      $add_shop->owner_name = $request->owner_name;
      $add_shop->owner_no = $request->owner_no;
      $add_shop->contact_person_name = $request->contact_person_name;
      $add_shop->contact_person_no = $request->contact_person_no;
      $add_shop->shop_mobile = $request->shop_mobile;
      $add_shop->shop_landline = $request->shop_landline;
      $add_shop->address = $request->address;
      $add_shop->store_logo = $photoToSave;
      $add_shop->email = $request->email;
      $add_shop->password = Hash::make($request->password);
      $add_shop->status = 'store';
      $add_shop->save();

      return redirect('/disc/stores/create');
    }

    public function couponValidate(Request $request)
    {
      // return $request;
      $mobile = $request->validate;
      $store = $request->store;
      $validation = MyCoupoun::where('user_phone', $mobile)->where('store', $store)->get();
      // return $validation;

      return view('validation')->with('valid', $validation);
    }
    public function edit($id)
    {
      $edit_id = Store::find($id);
      return view('stores.edit')->with('key', $edit_id);
    }
    public function update(Request $request)
    {
      $this->validate($request,[
        'name' => 'required',
        'contact_person_name' => 'required',
        'contact_person_no' => 'required',
        'address' => 'required',
      ]);
      $add_shop = Store::find($request->id);

      if ($request->hasFile('store_logo')) {
        $file = $request->store_logo;
        $fullPhotoNameWithExt = $file->getClientOriginalName();
        $fileName = pathinfo($fullPhotoNameWithExt, PATHINFO_FILENAME);
        $fileExt = $file->getClientOriginalExtension();
        $photoToSave = $fileName.'_'.time().'.'. $fileExt;
        $path = $file->move('shoplogo', $photoToSave);
        $add_shop->store_logo = $photoToSave;
      }
      $add_shop->name = $request->name;
      $add_shop->owner_name = $request->owner_name;
      $add_shop->owner_no = $request->owner_no;
      $add_shop->contact_person_name = $request->contact_person_name;
      $add_shop->contact_person_no = $request->contact_person_no;
      $add_shop->shop_mobile = $request->shop_mobile;
      $add_shop->shop_landline = $request->shop_landline;
      $add_shop->address = $request->address;
      // $add_shop->email = $request->email;
      // $add_shop->password = Hash::make($request->password);
      $add_shop->status = 'store';
      $add_shop->save();

      return redirect('/disc/stores/create');
      // return $request;
    }
    public function destroy($id)
    {
      $destroy_info = Store::find($id);
    $destroy_info->delete();
    return redirect('/disc/stores/create');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MyCoupoun;

class StoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:store');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // return 'tyuio';
        return view('validation.validation');
    }

    public function create()
    {   $list = Shop::all();
        return view('stores.create')->with('list', $list);
    }

    public function couponValidate(Request $request)
    {
      // return $request;
      $mobile = $request->validate;
      $store = $request->store;
      $validation = MyCoupoun::where('user_phone', $mobile)->where('store', $store)->get();
      // return $validation;

      return view('validation')->with('valid', $validation);
    }
}

<?php

namespace App\Http\Controllers;

use App\Shop;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function __construct()
     {
         $this->middleware('auth:admin');
     }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $list = Shop::all();
        return view('shops.create')->with('list', $list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'name' => 'required',
        'contact_person_name' => 'required',
        'contact_person_no' => 'required',
        'address' => 'required',
      ]);
      if ($request->hasFile('shop_logo')) {

        $file = $request->shop_logo;
        $fullPhotoNameWithExt = $file->getClientOriginalName();
        $fileName = pathinfo($fullPhotoNameWithExt, PATHINFO_FILENAME);
        $fileExt = $file->getClientOriginalExtension();
        $photoToSave = $fileName.'_'.time().'.'. $fileExt;
        $path = $file->move('shoplogo', $photoToSave);
      }
      $add_shop = new Shop();
      $add_shop->name = $request->name;
      $add_shop->owner_name = $request->owner_name;
      $add_shop->owner_no = $request->owner_no;
      $add_shop->contact_person_name = $request->contact_person_name;
      $add_shop->contact_person_no = $request->contact_person_no;
      $add_shop->shop_mobile = $request->shop_mobile;
      $add_shop->shop_landline = $request->shop_landline;
      $add_shop->address = $request->address;
      $add_shop->shop_logo = $photoToSave;
      $add_shop->save();

      return redirect('/disc/shops/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function show(Shop $shop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $list = Shop::find($id);
         return view('shops.edit')->with('key', $list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'name' => 'required',
        'contact_person_name' => 'required',
        'contact_person_no' => 'required',
        'address' => 'required',
      ]);
      if ($request->hasFile('shop_logo')) {

        $file = $request->shop_logo;
        $fullPhotoNameWithExt = $file->getClientOriginalName();
        $fileName = pathinfo($fullPhotoNameWithExt, PATHINFO_FILENAME);
        $fileExt = $file->getClientOriginalExtension();
        $photoToSave = $fileName.'_'.time().'.'. $fileExt;
        $path = $file->move('shoplogo', $photoToSave);
      }
      $edit_shop = shop::find($id);
      $edit_shop->name = $request->name;
      $edit_shop->owner_name = $request->owner_name;
      $edit_shop->owner_no = $request->owner_no;
      $edit_shop->contact_person_name = $request->contact_person_name;
      $edit_shop->contact_person_no = $request->contact_person_no;
      $edit_shop->shop_mobile = $request->shop_mobile;
      $edit_shop->shop_landline = $request->shop_landline;
      $edit_shop->address = $request->address;
        $edit_shop->shop_logo = $photoToSave;
      $edit_shop->save();

      return redirect('/disc/shops/create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $destroy_info = Shop::find($id);
    $destroy_info->delete();
    return redirect('/disc/shops/create');
    }
}

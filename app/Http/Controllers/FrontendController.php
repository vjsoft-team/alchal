<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Shop;
use App\Store;
use App\Category;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function lists()
    {
      $list = Coupon::all();
      return view('listings')->with('list', $list);
    }
    public function contactus()
    {

      return view('contactus');
    }
    public function searchStores($search)
    {
      $stores_list = Store::Where('name', $search)->get();
      $coupons_list = Coupon::Where('store', $stores_list->first()->id)->get();
        $data = [
          'key' => $search,
          'shop_list' => $stores_list,
          'list' => $coupons_list,
        ];
      return view('couponsonshop')->with($data);
    }
    public function categorysearch($search)
    {
      $coupons_list = Coupon::Where('category', $search)->get();
      $category_list = Category::Where('category', $search)->get();
        $data = [
          'key' => $search,
          'category_list' => $category_list,
          'list' => $coupons_list,
        ];
      return view('couponsonCategory')->with($data);
    }

    public function AllShops()
    {
      $shop_list = Store::orderBy('name', 'asc')->get();
      return view('AllStores')->with('list', $shop_list);
    }

    public function Terms()
    {
      return View('Terms');
    }
    public function Privacy()
    {
      return View('Privacy');
    }

}

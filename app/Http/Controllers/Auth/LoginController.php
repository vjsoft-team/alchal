<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use Exception;
use App\User;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest',['except'=>['logout','UserLogout']]);
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
      try {
          $user = Socialite::driver('facebook')->user();
      } catch (Exception $e) {
          return redirect('login/facebook');
      }

      // return var_dump($user->id);
      $authUser = User::where('facebook_id', $user->id)->first();
      // $authUser = $this->findOrCreateUser($user);

      if ($authUser) {
        // return $authUser;
        // if ($authUser->status != 1) {
        //   return redirect("/verifymobile/$authUser->mobile/");
        // } else {
          if (Auth::login($authUser)) {
            return redirect('/');
          } else {
            return redirect('/login');
          }
        // }

      } else {
        $regUser = User::create([
            'facebook_id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'status' => 1,
            'otp' => NULL,
            'password' => '',
        ]);

        if ($regUser) {
          Auth::login($regUser);
          return redirect('/');
        } else {
          return redirect('/register');
        }
        return redirect('/register');

        // return $regUser;

        // $logUser = User::find

      }
    }

   //  protected function credentials(Request $request)
   // {
   //     return ['email' => $request->{$this->username()}, 'password' => $request->password, 'status' => '1'];
   // }
   //
   // protected function sendFailedLoginResponse(Request $request)
   //  {
   //      $errors = [$this->username() => trans('auth.failed')];
   //
   //      // Load user from database
   //      $user = User::where($this->username(), $request->{$this->username()})->first();
   //
   //      // Check if user was successfully loaded, that the password matches
   //      // and active is not 1. If so, override the default error message.
   //      if ($user && \Hash::check($request->password, $user->password) && $user->status != 1) {
   //          $errors = [$this->username() => trans('auth.notactivated')];
   //      }
   //
   //      if ($request->expectsJson()) {
   //          return response()->json($errors, 422);
   //      }
   //      return redirect()->back()
   //          ->withInput($request->only($this->username(), 'remember'))
   //          ->withErrors($errors);
   //  }

    public function UserLogout()
    {
        Auth::guard('web')->logout();
        return redirect('/');
    }
}

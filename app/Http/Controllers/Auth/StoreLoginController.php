<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class StoreLoginController extends Controller
{
  public function __construct()
  {
    $this->middleware('guest:store', ['except'=>['storeLogout']]);
  }

  public function showLoginForm()
  {
    return view('auth.store_login');
  }
  public function Login(Request $request)
  {
    $this->validate($request,[
      'email' => 'required',
      'password' => 'required|min:5'
    ]);
    if (Auth::guard('store')->attempt(['email'=> $request->email, 'password'=> $request->password], $request->remember)) {
      return redirect()->intended(route('validation.dashboard'));
    }
    return redirect()->back()->withInput($request->only('email','remember'));
  }

  public function storeLogout()
  {
      Auth::guard('store')->logout();
      return redirect('/');
  }
}

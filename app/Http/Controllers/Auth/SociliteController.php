<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class SociliteController extends Controller
{

    public function savefacebookform()
    {
      return view('auth.facebookregister');
    }

    public function savefacebook(Request $request)
    {
      // return $request;
      $mobile = $request['mobile'];
      $rand = rand(111111, 999999);
      // $this->redirectTo = "/verifymobile/$mobile";
        User::create([
            'facebook_id' => $request['facebook_id'],
            'name' => $request['name'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'status' => 0,
            'otp' => $rand,
            'password' => bcrypt($request['password']),
        ]);

        return redirect("/verifymobile/$mobile");

    }
}

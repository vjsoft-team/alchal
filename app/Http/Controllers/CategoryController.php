<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth:admin');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list = Category::all();
        return view('categories.create')->with('list', $list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
          'category' => 'required',
          // 'category_image' => 'required'
        ]);

        $add = new Category();
        $add->category = $request->category;
        // $add->category_image = $request->category_image;
        $add->save();

        return redirect('/disc/category/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list = Category::find($id);
        return view('categories.edit')->with('key',$list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request ,$id)
    {
      $this->validate($request,[
        'category' => 'required',
        // 'category_image' => 'required'
      ]);

      $edit = Category::find($id);
      $edit->category = $request->category;
      // $edit->category_image = $request->category_image;
      $edit->save();

      return redirect('/disc/category/create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $destroy_info = Category::find($id);
    $destroy_info->delete();
    return redirect('/disc/category/create');
    }
}

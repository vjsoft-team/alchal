<?php

namespace App\Http\Controllers;

use App\Ads;
use Illuminate\Http\Request;

class AdsController extends Controller
{
      public function __construct()
      {
          $this->middleware('auth:admin');
      }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Ads::all();
        return view('ads.list')->with('list',$list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request;
        $this->validate($request,[
          'title' => 'required',
          'logo' => 'required',
          'cover_image' => 'required',
          'position' => 'required|unique:ads',
        ]);
        if ($request->hasFile('logo')) {

          $file = $request->logo;
          $fullPhotoNameWithExt = $file->getClientOriginalName();
          $fileName = pathinfo($fullPhotoNameWithExt, PATHINFO_FILENAME);
          $fileExt = $file->getClientOriginalExtension();
          $photoToSave = $fileName.'_'.time().'.'. $fileExt;
          $path = $file->move('sliders', $photoToSave);
        }
        if ($request->hasFile('cover_image')) {

          $file1 = $request->cover_image;
          $fullPhotoNameWithExt1 = $file1->getClientOriginalName();
          $fileName1 = pathinfo($fullPhotoNameWithExt1, PATHINFO_FILENAME);
          $fileExt1 = $file1->getClientOriginalExtension();
          $photoToSave1 = $fileName1.'_'.time().'.'. $fileExt1;
          $path1 = $file1->move('sliders', $photoToSave1);
        }
        $add = new Ads();
        $add->logo = $photoToSave;
        $add->cover_image = $photoToSave1;
        $add->title = $request->title;
        $add->description = $request->description;
        $add->position = $request->position;
        $add->save();
        return redirect('/disc/ads');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function show(Ads $ads)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $key = Ads::find($id);
        return view('ads.edit')->with('key', $key);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'title' => 'required',
        'position' => 'required',
      ]);
      $add = Ads::find($id);
      if ($request->hasFile('logo')) {

        $file = $request->logo;
        $fullPhotoNameWithExt = $file->getClientOriginalName();
        $fileName = pathinfo($fullPhotoNameWithExt, PATHINFO_FILENAME);
        $fileExt = $file->getClientOriginalExtension();
        $photoToSave = $fileName.'_'.time().'.'. $fileExt;
        $path = $file->move('sliders', $photoToSave);
        $add->logo = $photoToSave;
      }
      if ($request->hasFile('cover_image')) {

        $file1 = $request->cover_image;
        $fullPhotoNameWithExt1 = $file1->getClientOriginalName();
        $fileName1 = pathinfo($fullPhotoNameWithExt1, PATHINFO_FILENAME);
        $fileExt1 = $file1->getClientOriginalExtension();
        $photoToSave1 = $fileName1.'_'.time().'.'. $fileExt1;
        $path1 = $file1->move('sliders', $photoToSave1);
        $add->cover_image = $photoToSave1;
      }

      $add->title = $request->title;
      $add->description = $request->description;
      $add->position = $request->position;
      $add->save();
      return redirect('/disc/ads');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $destroy_info = Ads::find($id);
    $destroy_info->delete();
    return redirect('/disc/ads');
    }
}

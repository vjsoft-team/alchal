<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\mail\SendMail;

class ContactusController extends Controller
{
  public function send()
 {
   Mail::send(new SendMail());
   return redirect('/thank') ;

 }

 public function contact()
 {
   return view('contactus');
 }
 public function thank()
 {
   return view('thanku');
 }
}

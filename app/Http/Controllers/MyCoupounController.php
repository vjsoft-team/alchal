<?php

namespace App\Http\Controllers;

use App\MyCoupoun;
use App\Coupon;
use Auth;
use Illuminate\Http\Request;

class MyCoupounController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $couponCode = $request->couponCode;
        // $mobile = $request->userNumber;
        // $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=alchal&password=smspassword&to=$mobile&from=Alchal&message=$couponCode";
        // file_get_contents($url);
        $findStore = MyCoupoun::where('user_id', $request->userId)->where('coupon_id', $request->couponId)->first();
        if(count($findStore)) {
          $message = [
            'status' => 'ok'
          ];
        } else {
          $newMyCoupoun = new MyCoupoun;
          $newMyCoupoun->store = $request->store;
          $newMyCoupoun->coupon_id = $request->couponId;
          $newMyCoupoun->coupon_code = $request->couponCode;
          $newMyCoupoun->user_id = $request->userId;
          $newMyCoupoun->user_phone = Auth::user()->mobile;
          $newMyCoupoun->save();
          $updateusedcoupon = Coupon::find($request->couponId);
          // $prev_usedValue = $updateusedcoupon->used;
          $update = $updateusedcoupon->used + 1;
          $updateusedcoupon->used = $update;
          $updateusedcoupon->save();
          // return $message = $updateusedcoupon;
          $message = [
            'status' => 'ok'
          ];
        }
        return $message;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MyCoupoun  $myCoupoun
     * @return \Illuminate\Http\Response
     */
    public function show(MyCoupoun $myCoupoun)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MyCoupoun  $myCoupoun
     * @return \Illuminate\Http\Response
     */
    public function edit(MyCoupoun $myCoupoun)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MyCoupoun  $myCoupoun
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MyCoupoun $myCoupoun)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MyCoupoun  $myCoupoun
     * @return \Illuminate\Http\Response
     */
    public function destroy(MyCoupoun $myCoupoun)
    {
        //
    }
}

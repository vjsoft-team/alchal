<?php

namespace App\Http\Controllers;

use App\Favouritestore;
use Illuminate\Http\Request;

class FavouritestoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $findStore = Favouritestore::where('user_id', $request->user_id)->where('store_id', $request->store_id)->first();
        // $findStore->where('store_id', $request->store_id);
        // $findStore->get();


        if(count($findStore)) {
          $findStore->delete();
          $message = [
            'status' => 'deleted'
          ];
          return $message;
        }

        $newFavouriteStore = new Favouritestore;
        $newFavouriteStore->user_id = $request->user_id;
        $newFavouriteStore->store_id = $request->store_id;
        $newFavouriteStore->save();
        $message = [
          'status' => 'ok'
        ];
        return $message;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Favouritestore  $favouritestore
     * @return \Illuminate\Http\Response
     */
    public function show(Favouritestore $favouritestore)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Favouritestore  $favouritestore
     * @return \Illuminate\Http\Response
     */
    public function edit(Favouritestore $favouritestore)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Favouritestore  $favouritestore
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Favouritestore $favouritestore)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Favouritestore  $favouritestore
     * @return \Illuminate\Http\Response
     */
    public function destroy(Favouritestore $favouritestore)
    {
        //
    }
}

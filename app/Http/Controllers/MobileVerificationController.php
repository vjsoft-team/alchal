<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class MobileVerificationController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($mobile)
  {
      // return $mobile;
      $oldRand = User::where('mobile', $mobile)->first();
      if ($oldRand->status == 1) {
        return redirect('/login');
      }
      $tableRand = $oldRand->otp;
      if ($oldRand->messageStatus == 0 && $oldRand->status == 0) {
        $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=alchal&password=smspassword&to=$mobile&from=Alchal&message=$tableRand";
        file_get_contents($url);
      } else {
        $rand = rand(111111, 999999);
      }
      $oldRand->messageStatus = 1;
      $oldRand->save();

      // return $oldRand->messageStatus;

      // $newRand = $oldRand->otp;
      return view('mobileverify')->with('mobile', $mobile);
  }

  public function verifymobileemail($email)
  {
      // return $mobile;
      $oldRand = User::where('email', $email)->first();
      return redirect("verifymobile/$oldRand->mobile") ;
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function resend(Request $request)
  {
    // return $request;
    $rand = rand(111111, 999999);
    $mobile = $request['mobile'];
    $oldRand = User::where('mobile', $mobile)->first();
    $oldRand->otp = $rand;
    $oldRand->save();
    $tableRand = $oldRand->otp;

    $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=vjsoft&password=vj@123&to=$mobile&from=vJsOFT&message=$rand";
    file_get_contents($url);
    return redirect("verifymobile/$mobile");

  }

  public function store(Request $request)
  {
    // return $request;
      $mobile = $request['mobile'];
      $otp = $request['otp'];
      $user = User::where('mobile', $mobile)->where('otp', $otp)->get();
      // return $user;
      if ($user->count()) {
        $user->first()->status = 1;
        $user->first()->save();
        $userEmail = $user->first()->email;
        $message = "Mobile Number Verified Successfully, Please Login!";
        $data = [
          'message' => $message,
          'useremail' => $userEmail
        ];

        return view('auth.login')->with('data', $data);
        // return $user;
      } else {
        $message = "Wrong Otp";
        return redirect()->back()->with('status', "Wrong Otp");
      }

  }
}

@php
  use App\Coupon;
  use App\Shop;
  use App\Ads;
  use App\Category;
  $pageId = 4;
@endphp
  <!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codenpixel.com/demo/kuponhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:53:44 GMT -->
<head>
      <meta charset="utf-8" />
      <title>Contact Alchal</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta content="" name="description" />
      <meta content="Kupons" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="#">
      <link href="{{config('APP.URL')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animate.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animsition.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css">
      <!-- Theme styles -->
      <link href="{{config('APP.URL')}}/assets/css/style.css" rel="stylesheet" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!-- Navigation Bar-->
         @include('backend.includes.header')
         <!-- Navigation ends -->
         <div class="wrapper">
           <section class="contact m-t-30">
              <div class="container">
                 <div class="row">
                    <!-- REGISTER -->
                    <div class="col-md-8">
                       <div class="widget">
                          <div class="widget-body">
                             <form class="form-horizontal" action="{{route('send')}}" method="post">
                               {{ csrf_field() }}
                                <fieldset>
                                   <div class="form-group required">
                                      <label class="col-md-3 control-label">You are a <sup>*</sup>
                                      </label>
                                      <div class="col-md-7">

                                         <div class="radio radio-success radio-single">
                                           <input type="radio" id="singleRadio1" value="option1" name="purpose" >
                                           <label for="singleRadio1">Business</label> <br>
                                            <input type="radio" id="singleRadio2" value="option4" name="purpose" checked="checked" >
                                            <label for="singleRadio2">Individual</label>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="form-group required">
                                      <label class="col-md-3 control-label">First Name <sup>*</sup>
                                      </label>
                                      <div class="col-md-7">
                                         <input name="name" placeholder="" class="form-control input-md" required="" type="text">
                                      </div>
                                   </div>
                                   <div class="form-group required">
                                      <label class="col-md-3 control-label">Last Name <sup>*</sup>
                                      </label>
                                      <div class="col-md-7">
                                         <input name="textinput" placeholder="" class="form-control input-md" type="text">
                                      </div>
                                   </div>
                                   <div class="form-group required">
                                      <label for="inputEmail3" class="col-md-3 control-label">Email <sup>*</sup>
                                      </label>
                                      <div class="col-md-7">
                                         <input type="email" class="form-control" id="inputEmail3" name="email" placeholder="">
                                      </div>
                                   </div>
                                   <div class="form-group required">
                                      <label class="col-md-3 control-label">Mesaage</label>
                                      <div class="col-md-7">
                                         <textarea class="form-control" cols="40" id="Description" name="description" rows="10"></textarea>
                                      </div>
                                   </div>
                                   <div class="form-group">
                                      <label class="col-md-3 control-label"></label>
                                      <div class="col-md-7">
                                         <div class="termbox mb10">
                                            <div class="radio radio-success radio-single">
                                               <input type="radio" id="singleRadio3" value="option3" name="radioSingle2" aria-label="Single radio One">
                                               <label>I have read and agree to the <a href="#">Terms &amp; Conditions</a>
                                               </label>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="form-group m-top-30">
                                      <div class="col-md-3"></div>
                                      <div class="col-md-7">
                                         <div class="clearfix"></div>
                                         <input type="submit" name="submit" class="btn btn-danger btn-raised legitRipple" value="Send message">
                                         {{-- <a class="btn btn-danger btn-raised legitRipple" href="#">Send message</a> --}}
                                      </div>
                                   </div>
                                </fieldset>
                             </form>
                          </div>
                       </div>
                       <!-- end: Widget -->
                    </div>
                    <!-- /REGISTER -->
                    <!-- WHY? -->
                    <div class="col-md-4">
                       <h3>Contact Us</h3>
                       <p>Once you're registered, you can:</p>

                       <hr>
                       <div class="panel">
                          <div class="panel-heading">
                             <h4 class="panel-title"><a data-parent="#accordion" data-toggle="collapse" class="panel-toggle collapsed" href="#faq1" aria-expanded="false"><i class="ti-info-alt" aria-hidden="true"></i>Office- 0877-2251599</a></h4>
                          </div>
                          <div class="panel-collapse collapse" id="faq1" aria-expanded="false" role="article" style="height: 0px;">
                             {{-- <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum ut erat a ultricies. Phasellus non auctor nisi, id aliquet lectus. Vestibulum libero eros, aliquet at tempus ut, scelerisque sit amet nunc. Vivamus id porta neque, in pulvinar ipsum. Vestibulum sit amet quam sem. Pellentesque accumsan consequat venenatis. Pellentesque sit amet justo dictum, interdum odio non, dictum nisi. Fusce sit amet turpis eget nibh elementum sagittis. Nunc consequat lacinia purus, in consequat neque consequat id.</div> --}}
                          </div>
                       </div>
                       <!-- end:panel -->
                       <div class="panel">
                          <div class="panel-heading">
                             <h4 class="panel-title"><a data-parent="#accordion" data-toggle="collapse" class="panel-toggle collapsed" href="#faq2" aria-expanded="false"><i class="ti-info-alt" aria-hidden="true"></i>Business queries: 9177790575</a></h4>
                          </div>
                          <div class="panel-collapse collapse" id="faq2" aria-expanded="false" role="article" style="height: 0px;">
                             {{-- <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum ut erat a ultricies. Phasellus non auctor nisi, id aliquet lectus. Vestibulum libero eros, aliquet at tempus ut, scelerisque sit amet nunc. Vivamus id porta neque, in pulvinar ipsum. Vestibulum sit amet quam sem. Pellentesque accumsan consequat venenatis. Pellentesque sit amet justo dictum, interdum odio non, dictum nisi. Fusce sit amet turpis eget nibh elementum sagittis. Nunc consequat lacinia purus, in consequat neque consequat id.</div> --}}
                          </div>
                       </div>
                       <!-- end:Panel -->
                       <hr>
                       <h4 class="m-t-20">Head Office</h4>
                       <p>Plot No : 104 </p>
                       <p>Mata Chowk -Rangpuri 110033- New Delhi , India </p>
                       <h4 class="m-t-20">Rigesterd Office</h4>
                       <p>Plot No : 301/3 </p>
                       <p>Kazana Jewelry Upstairs</p>
                       <p>V.V.Mahal Road</p>
                       <p>Tirupati - 517501</p>
                       <p>Andra Pradesh</p>
                    </div>
                    <!-- /WHY? -->
                 </div>
              </div>
           </section>


            <!-- Footer -->
            @include('backend.includes.mainfooter')
            <!-- start modal -->
           <!-- Large modal -->
           @php

           @endphp

           @php
             $coupons_list = Coupon::all();
           @endphp
           @foreach ($coupons_list as $coupon)
             <div class="coupon_modal modal fade couponModal{{$coupon->id}}" tabindex="-1" role="dialog" style="z-index: 99999">
                <div class="modal-dialog modal-lg" role="document">
                   <div class="modal-content">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ti-close"></i></span> </button>
                      <div class="coupon_modal_content">

                         <div class="row">
                            <div class="col-sm-10 col-sm-offset-1 text-center">
                               <h2>{{$coupon->title}}</h2>
                               <p>Not applicable to ICANN fees, taxes, transfers,or gift cards. Cannot be used in conjunction with any other offer, sale, discount or promotion. After the initial purchase term.</p>
                            </div>

                         <div class="row">
                             <div class="col-sm-12">
                                 <h5 class="text-center text-uppercase m-t-20 text-muted">Click below to get your coupon code</h5>
                             </div>

                            <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                              <a href="#" target="_blank" class="coupon_code alert alert-info">
                                <span class="coupon_icon"><i class="ti-cut hidden-xs"></i></span>  {{$coupon->coupon}}
                               </a>
                            </div>
                         </div>
                            <div class="row">
                               <div class="col-sm-12">
                                  <div class="report">Did this coupon work? <span class="yes vote-link" data-src="#">Yes</span> <span class="no vote-link" data-src="#">No</span> </div>
                               </div>
                            </div>
                         </div>
                      </div>
                      <!-- end: Coupon modal content -->
                   </div>



                        <div class="newsletter-modal">
                         <div class="newsletter-form">
                            <h4><i class="ti-email"></i>Sign up for our weekly email newsletter with the best money-saving coupons.</h4>
                            <div class="input-group">
                               <input class="form-control input-lg" placeholder="Email" type="text"> <span class="input-group-btn">
                               <button class="btn btn-danger btn-lg" type="button">
                               Subscribe
                               </button>
                               </span>
                            </div>
                            <p><small>We’ll never share your email address with a third-party.</small> </p>
                            </div>
                        </div>
                         <ul class="nav nav-pills nav-justified">
                      <li role="presentation" class="active"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It worked"><i class="ti-check color-green"></i></a> </li>
                      <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="I love it"><i class="ti-heart color-primary"></i></a> </li>
                      <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-close"></i></a> </li>
                   </ul>


                </div>
             </div>
           @endforeach

         <!-- end: Modall -->
         </div>
      </div>
      <!-- //wrapper -->
      <!-- jQuery  -->
      <script src="{{config('APP.URL')}}/assets/js/jquery.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/bootstrap.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/animsition.min.js"></script>
      <script src="{{config('APP.URL')}}/owl.carousel/owl.carousel.min.js"></script>
      <!-- Kupon js -->
      <script src="{{config('APP.URL')}}/assets/js/kupon.js"></script>
   </body>

<!-- Mirrored from codenpixel.com/demo/kuponhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:54:20 GMT -->
</html>

<nav class="page-sidebar" data-pages="sidebar">

    <div class="sidebar-overlay-slide from-top" id="appMenu">
        <div class="row">
            <div class="col-xs-6 no-padding">
                <a href="#" class="p-l-40"><img src="{{config('APP_URL')}}/backend/assets/img/demo/social_app.svg" alt="socail">
                </a>
            </div>
            <div class="col-xs-6 no-padding">
                <a href="#" class="p-l-10"><img src="{{config('APP_URL')}}/backend/assets/img/demo/email_app.svg" alt="socail">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 m-t-20 no-padding">
                <a href="#" class="p-l-40"><img src="{{config('APP_URL')}}/backend/assets/img/demo/calendar_app.svg" alt="socail">
                </a>
            </div>
            <div class="col-xs-6 m-t-20 no-padding">
                <a href="#" class="p-l-10"><img src="{{config('APP_URL')}}/backend/assets/img/demo/add_more.svg" alt="socail">
                </a>
            </div>
        </div>
    </div>

    <div class="sidebar-header">
        <img src="{{config('APP_URL')}}/backend/assets/img/logo_white.png" alt="logo" class="brand" data-src="{{config('APP_URL')}}/backend/assets/img/logo_white.png" data-src-retina="{{config('APP_URL')}}/backend/assets/img/logo_white_2x.png" width="78" height="22">
        <div class="sidebar-header-controls">
            <button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu"><i class="fa fa-angle-down fs-16"></i>
            </button>
            <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
            </button>
        </div>
    </div>

    <div class="sidebar-menu">

        <ul class="menu-items">
            <li class="m-t-30">
                <a href="/disc" class="detailed">
                    <span class="title">Dashboard</span>
                    <span class="details">12 New Updates</span>
                </a>
                <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
            </li>
            <li class="m-t-30">
                <a href="{{ config('app.url') }}validation/coupons/create" class="detailed">
                    <span class="title">Coupons</span>

                </a>
                <span class="icon-thumbnail">+C</span>
            </li>


            {{--
            <li>
                <a href="javascript:;"><span class="title">Add Inventory</span> <span class="arrow"></span></a>
                <span class="icon-thumbnail">AI</span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="/products/create">Add Products</a>
                        <span class="icon-thumbnail">AP</span>
                    </li>

                    <li class="">
                        <a href="/stock/create">Add Stock</a>
                        <span class="icon-thumbnail">ST</span>
                    </li>
                    <li class="">
                        <a href="/gatepass/create">Gate Pass</a>
                        <span class="icon-thumbnail">GP</span>
                    </li> --}} {{--
                    <li class="">
                        <a href="/qualification/create">Add Qualification</a>
                        <span class="icon-thumbnail">AQ</span>
                    </li>
                    <li class="">
                        <a href="/qualification">Qualification List</a>
                        <span class="icon-thumbnail">QL</span>
                    </li>
                    <li class="">
                        <a href="/subcastes/create">Add Sub-Caste</a>
                        <span class="icon-thumbnail">SC</span>
                    </li>
                    <li class="">
                        <a href="/subcastes">Sub-Castes List</a>
                        <span class="icon-thumbnail">QL</span>
                    </li> --}}

                </ul>
            </li>

            <li>
                <a href="javascript:;"><span class="title">Reports</span> <span class="arrow"></span></a>
                <span class="icon-thumbnail"><i class="pg-tables"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="/products">Stock</a>
                        <span class="icon-thumbnail">SR</span>
                    </li>
                    <li class="">
                        <a href="/sales">Sales</a>
                        <span class="icon-thumbnail">SR</span>
                    </li>
                    <li class="">
                        <a href="/gatepassreports">Gatepass</a>
                        <span class="icon-thumbnail">GR</span>
                    </li>
                </ul>
            </li>

        </ul>
        <div class="clearfix"></div>
    </div>

</nav>


  <!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codenpixel.com/demo/kuponhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:53:44 GMT -->
<head>
      <meta charset="utf-8" />
      <title>{{config('app.name')}}</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta content="" name="description" />
      <meta content="Kupons" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="#">
      <link href="{{config('APP.URL')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animate.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animsition.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css">
      <!-- Theme styles -->
      <link href="{{config('APP.URL')}}/assets/css/style.css" rel="stylesheet" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!-- Navigation Bar-->
         @include('backend.includes.header')
         <!-- Navigation ends -->
         <div class="wrapper">
           <section class="contact m-t-30">
              <div class="container">
                 <div class="row">
                    <!-- REGISTER -->
                    <div class="col-md-12">
                       <div class="widget">

                          <div class="widget-body">
                            <h3>Privacy Policy</h3>

                            <br>
                              <p>Use of the Site is governed by our Privacy Policy, which can be located at</p><br>
                              <p>https://alchal.com/privacy-policy</p><br>
                              <h4>MANDATORY DECLARATION</h4><br>
                              <p>1. You hereby declare that you have provided all necessary Legal Details for the
shipment of the product to the destination specified by you in the Order.</p><br>
<p>2. You further declare that the Product will be acquired for personal use of the
Consignee and/or Buyer. The Product will not be sold, resold, bartered or in any way
disposed for profit.</p><br>
<h4>Disclaimer Of Warranties And Liability</h4><br>
<p>This Website, all the materials and products (including but not limited to software) and
services, included on or otherwise made available to you through this site are provided
by Alchal on an “as is” and “as available” basis without any representation or warranties,
express or implied except otherwise specified in writing. Without prejudice to the
forgoing paragraph, Alchal does not warrant that:</p><br>
<p>1. This website will be constantly available, or available at all; or</p><br>
<p>2. The information on this website is complete, true, accurate or non-misleading.
</p><br>
<p>Alchal will not be liable to you in any way or in relation to the contents of, or use of, or
otherwise in connection with, this website.</p><br>
<p>Alchal does not warrant that this site; information, content, materials, product (including
software) or services included on or otherwise made available to you through this site;
their servers; or electronic communication sent from Alchal are free of viruses or other
harmful components.
</p><br>
<p>Nothing on this website constitutes, or is meant to constitute, advice of any kind.</p><br>
<p>Certain state laws do not allow limitations on implied warranties or the exclusion or
limitation of certain damages. If these laws apply to you, some or all of the above
disclaimers, exclusions, or limitations may not apply to you, and you might have
additional rights.
</p><br>
<p>You will be required to enter a valid phone number while placing an order on the
Website. By registering your phone number with us, you consent to be contacted by
Alchal via phone calls and / or SMS notifications, in case of any order or shipment or
delivery related updates. Alchal will not use your personal information to initiate any
promotional phone calls or SMS'es.</p><br>
<p>While availing any of the payment method/s offered by Alchal, Alchal is not responsible
or takes no liability of whatsoever nature in respect of any loss or damage arising
directly or indirectly to you out of the decline due to :</p><br>
<p>1. lack of authorization for any transaction/s,</p><br>
<p>2. or exceeding the preset limit mutually agreed by you and between your "Bank/s",</p><br>
<p>3. or any payment issues arising out of the transaction,
</p><br>
<p>4. or decline of transaction for any other reason/s.
</p><br>
<h4>Applicable Law</h4><br>
<p>This Agreement shall be governed by and interpreted and construed in accordance with
the laws of India. The place of jurisdiction shall be in Tirupati. Jurisdictional Issues/Sale
in India Only</p><br>
<p>Unless otherwise specified, the material on the Website is presented solely for the
purpose of sale India. Alchal makes no representation that materials on the Website are
appropriate or available for use in other locations/Countries other than India. Those who
choose to access this site from other locations/Countries other than India do so on their
own initiative and Alchal is not responsible for supply of goods/refund for the goods
ordered from other locations/Countries other than India, compliance with local laws, if
and to the extent local laws are applicable.</p><br>
<h4>Trademark, Copyright and Restriction</h4><br>
<p>This site is controlled and operated by Alchal. All material on this site, including images,
illustrations, audio clips, and video clips, are protected by copyrights, trademarks, and
other intellectual property rights that are owned and controlled by us or by other parties
that have licensed their material to us. Material on Website owned, operated, licensed
or controlled by us is solely for your personal, non-commercial use. You must not copy,
reproduce, republish, upload, post, transmit or distribute such material in any way,
including by e-mail or other electronic means and whether directly or indirectly and you
must not assist any other person to do so. Without the prior written consent of the
owner, modification of the materials, use of the materials on any other web site or
networked computer environment or use of the materials for any purpose other than
personal, non-commercial use is a violation of the copyrights, trademarks and other
proprietary rights, and is prohibited. Any use for which you receive any remuneration,
whether in money or otherwise, is a commercial use for the purposes of this clause.</p><br>
<h4>Your Account</h4><br>
<p>You agree that any use on the site is at your own risk and Alchal is not responsible for
it. You are responsible for maintaining the confidentiality of your account and password
and for restricting access to your computer, and you agree to accept responsibility for all
activities that occur under your account or password. Products sold on the Website are
not for people below aged 18, however any products available for children below age
18, can be bought by adults.
</p><br>
<h4>Copyright complaint</h4><br>
<p>We at Alchal respect the intellectual property of others. In case you feel that your work
has been copied in a way that constitutes copyright infringement you can write to us at
legal@Alchal.com.</p><br>
<h4>Risk of loss</h4><br>
<p>All items purchased from the Website are made pursuant to a shipment contract. This
means that the risk of loss and title for such items pass to you upon our delivery to the
carrier.
</p><br>
<h4>Product Description</h4><br>
<p>Alchal tries to be as accurate as possible. However, Alchal does not warrant that
product description or other content of this site is accurate, complete, reliable, current,
or error-free.
</p><br>
<h4>Pricing/Typographical error</h4><br>
<p>Pricing on the product is as it shows, however due to any technical issue, typographical
error or product information received from our suppliers' prices of the product may vary.
Alchal will try to be accurate, however if you come across any difference in pricing on
the Website pre and post orders, we request you to contact us within 72 (Seventy Two)
hours of placing your order. Alchal shall then refund the difference amount within 10-15
business days of intimation. The mode of refund will be the same as at the time of
placing the order or as specified in the refund policy. Cash on Delivery order refunds will
be done by Cheque. If Alchal comes across any such difference in pricing, it has all the
rights to rectify the same or cancel the order.</p><br>
<h4>REFUNDS & CANCELLATIONS</h4><br>
<p>Alchal hopes that you never have to cancel or return a product. At the same time, we
would be there to help you whenever you do so due to certain circumstances. As a first
step we are laying out our cancellation and return policy to take care of your hassles.</p><br>
<p>ORDER HAS NOT BEEN SHIPPED YET: Any order which has not been shipped can
be cancelled partially or fully. The amount paid by you (if it is a prepaid order) would be
refunded to you and should reflect in your account in 7-10 days.
</p><br>
<p>ORDER WAS DAMAGED WHEN RECEIVED: We mandate that stores listed on our
website take utmost care while packaging to ensure that the product reaches you the
way you had seen it on our website. However, due to situations beyond control, the
items delivered might get damaged in transit. The stores would replace or cancel your
order under such circumstance. We would request you to inform us and the stores of
the damage within 48 hours of receipt of order.. Any refund would happen only after
proper check of the damaged material.
</p><br>
<p>INCORRECT ITEMS RECEIVED: We are proud of the robustness of our fulfilment
process and the right shipment would reach the right person at the right place. In case
you receive items that are different from the ones you ordered, we will require the stores
to ship the right items to you right away. Please send us an email at
customercare@Alchal.com with the order ID and complaint details. We would respond
to it in 48 hours. We would try and reverse pick up the product from your location
wherever possible, at other locations we will try and find a mutually agreeable solution.</p><br>
<p>Refund would be processed after the item has been shipped back or picked up from
customer location.</p><br>
<p>Any other reasons for cancellation or return would be handled on a case to case basis
</p><br>
<p>Following shall not be eligible for return or replacement</p><br>
<p>1. Damages due to misuse of product</p><br>
<p>2. Incidental damage due to malfunctioning of product</p><br>
<p>3. Any consumable item which has been used or installed</p><br>
<p>4. Products with tampered or missing serial / UPC numbers</p><br>
<p>5. Any damage / defect which are not covered under the manufacturer's warranty</p><br>
<p>6. Any product that is returned without all original packaging and accessories, including
the box, manufacturer's packaging if any, and all other items originally included with the
product(s) delivered</p><br>
<p>Refunds are made as follows: 1. If the payment was made in cash then the refund is
made in the form of coupon codes 2. If the payment is made using a credit/debit card
(or any form of net banking) then the amount is credited back to the same account from
which the payment was made.</p><br>
<h4>Cancellation of order</h4><br>
<p>Alchal reserves the right to cancel any order without any explanation for doing so, under
situation where Alchal is not able to meet the requirement of the order placed or order
so placed/cancelled does not comply with the Website policy or for any other reason.
However, Alchal will ensure that any communication of cancellation of an order, so
cancelled, is intimated within appropriate time to the concerned person and any
applicable refund, will be made in reasonable time.</p><br>
<h4>Applicable Law</h4><br>
<p>This Agreement shall be governed by and interpreted and construed in accordance with
the laws of India. The place of jurisdiction shall be in Tirupati.
</p><br>
<h4>Jurisdictional Issues/Sale in India Only</h4><br>
<p>Unless otherwise specified, the material on the Website is presented solely for the
purpose of sale India. Alchal makes no representation that materials in the Website are
appropriate or available for use in other locations/Countries other than India. Those who
choose to access this site from other locations/Countries other than India do so on their
own initiative and Alchal is not responsible for supply of goods/refund for the goods
ordered from other locations/Countries other than India, compliance with local laws, if
and to the extent local laws are applicable.
</p><br>
<h4>Trademark, Copyright and Restriction</h4><br>
<p>This site is controlled and operated by Alchal. All material on this site, including images,
illustrations, audio clips, and video clips, are protected by copyrights, trademarks, and
other intellectual property rights that are owned and controlled by us or by other parties
that have licensed their material to us. Material on the Web site owned, operated,
licensed or controlled by us is solely for your personal, non-commercial use. You must
not copy, reproduce, republish, upload, post, transmit or distribute such material in any
way, including by e-mail or other electronic means and whether directly or indirectly and
you must not assist any other person to do so. Without the prior written consent of the
owner, modification of the materials, use of the materials on any other web site or
networked computer environment or use of the materials for any purpose other than
personal, non-commercial use is a violation of the copyrights, trademarks and other
proprietary rights, and is prohibited. Any use for which you receive any remuneration,
whether in money or otherwise, is a commercial use for the purposes of this clause.
</p><br>
<h4>Your Account</h4><br>
<p>You agree that any use on the site is at your own risk and Alchal is not responsible for
it. You are responsible for maintaining the confidentiality of your account and password
and for restricting access to your computer, and you agree to accept responsibility for all
activities that occur under your account or password. Products sold on the Website are
not for people below aged 18, however any products available for children below age
18, can be bought by adults.</p><br>
<h4>Copyright Complaint</h4>
<p>We at Alchal respect the intellectual property of others. In case you feel that your work
has been copied in a way that constitutes copyright infringement you can write to us at
legal@Alchal.com.</p><br>
<h4>Risk of loss
</h4><br>
<p>All items purchased from the Website are made pursuant to a shipment contract. This
means that the risk of loss and title for such items pass to you upon our delivery to the
carrier</p><br>
<h4>Our Contact
</h4><br>
<p>Please send any questions or comments (including all inquiries unrelated to copyright
infringement) regarding this Site to customercare@Alchal.com</p>
                          </div>
                       </div>
                       <!-- end: Widget -->
                    </div>
                    <!-- /REGISTER -->
                    <!-- WHY? -->

                    <!-- /WHY? -->
                 </div>
              </div>
           </section>


            <!-- Footer -->
            @include('backend.includes.mainfooter')
            <!-- start modal -->
           <!-- Large modal -->


         <!-- end: Modall -->
         </div>
      </div>
      <!-- //wrapper -->
      <!-- jQuery  -->
      <script src="{{config('APP.URL')}}/assets/js/jquery.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/bootstrap.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/animsition.min.js"></script>
      <script src="{{config('APP.URL')}}/owl.carousel/owl.carousel.min.js"></script>
      <!-- Kupon js -->
      <script src="{{config('APP.URL')}}/assets/js/kupon.js"></script>
   </body>

<!-- Mirrored from codenpixel.com/demo/kuponhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:54:20 GMT -->
</html>

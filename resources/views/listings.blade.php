@php
  use App\Coupon;
  use App\Shop;
  use App\Store;
  use App\Ads;
  use App\Category;
  $pageId = 2;
@endphp
  <!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codenpixel.com/demo/kuponhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:53:44 GMT -->
<head>
      <meta charset="utf-8" />
      <title>Alchal Coupons</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta content="" name="description" />
      <meta content="Kupons" name="author" />
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="#">
      <link href="{{config('APP.URL')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animate.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animsition.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css">
      <!-- Theme styles -->
      <link href="{{config('APP.URL')}}/assets/css/style.css" rel="stylesheet" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!-- Navigation Bar-->
         @include('backend.includes.header')
         <!-- Navigation ends -->
         <div class="wrapper">
            <div class="container">

              <div class="row">

                  <div class="col-sm-3">

                      <div class="widget">
                          <!-- /widget heading -->
                          <div class="widget-heading"><br><br>
                              <h3 class="widget-title text-dark">
                            Search your Favourite Stores
                         </h3>
                              <div class="clearfix"></div>
                          </div>
                          <div class="widget-body">
                              <form class="form-horizontal select-search">
                                  <label class="control-label">What you searching for?</label>
                                  <div class="btn-group" data-toggle="buttons">
                                      <label class="btn btn-default active"> <i class="ti-tag"></i>
                                          <input type="checkbox" checked="">Coupons</label>
                                      <label class="btn btn-default"> <i class="ti-cut"></i>
                                          <input type="checkbox">Discounts</label>
                                      <label class="btn btn-default"> <i class="ti-alarm-clock"></i>
                                          <input type="checkbox">Deals</label>
                                  </div>
                                  <fieldset>
                                      <div class="form-group">
                                          <label class="control-label">Keyword</label>
                                          <input class="form-control" id="text" name="text" type="text"> </div>
                                      <div class="row">
                                          <!-- Select Basic -->
                                          <div class="form-group col-sm-6 col-xs-12">
                                              <label class="control-label " for="category">Select category</label>
                                              <select class="select form-control" id="category" name="category">
                                                  <option value="Electronics">Electronics</option>
                                                  <option value="Fashion">Fashion</option>
                                                  <option value="Kids">Kids</option>
                                              </select>
                                          </div>
                                          <div class="form-group col-sm-6 col-xs-12">
                                              <label class="control-label " for="store">Select a store</label>
                                              <select class="select form-control" id="store" name="store">
                                                  <option value="Store">Store</option>
                                                  <option value="Ebay">Ebay</option>
                                                  <option value="Ebay">Shopname</option>
                                                  <option value="Ebay">Hostgator</option>
                                                  <option value="Ebay">Ebay</option>
                                                  <option value="Bangdoo">Bangdoo</option>
                                              </select>
                                          </div>
                                      </div>
                                      <!-- //row -->
                                      <!-- Button -->
                                      <div class="form-group ">
                                          <button id="search_btn" name="search_btn" class="btn btn-success">Search coupons</button>
                                      </div>
                                  </fieldset>
                              </form>
                          </div>
                      </div>
                      <!--/search form -->
                      {{-- <div class="widget trending-coupons">
                          <!-- /widget heading -->
                          <div class="widget-heading">
                              <h3 class="widget-title text-dark">
                            Trending Coupons
                         </h3>
                              <div class="clearfix"></div>
                          </div>
                          <div class="widget-body">
                              <div class="media">
                                  <div class="media-left media-middle"> <img src="assets/images/thmbsm-5.png" alt=""> </div>
                                  <div class="media-body">
                                      <h4 class="media-heading">Upto 70% Rewards</h4>
                                      <p>Up to 70% off on Clothing ...</p>
                                  </div>
                              </div>
                              <!--/coupon media -->
                              <div class="media">
                                  <div class="media-left media-middle"> <img src="assets/images/thmbsm-6.png" alt=""> </div>
                                  <div class="media-body">
                                      <h4 class="media-heading">Upto 70% Rewards</h4>
                                      <p>Up to 70% off on Clothing ...</p>
                                  </div>
                              </div>
                              <!--/coupon media -->
                              <div class="media">
                                  <div class="media-left media-middle"> <img src="assets/images/thmbsm-4.png" alt=""> </div>
                                  <div class="media-body">
                                      <h4 class="media-heading">Up to 50% off Mens Summer Essentials at Clothing</h4>
                                      <p>Up to 70% off on Clothing ...</p>
                                  </div>
                              </div>
                          </div>
                          <!-- // widget body -->
                      </div> --}}
                      <!-- //trending -->
                      {{-- <div class="widget">
                          <!-- /widget heading -->
                          <div class="widget-heading">
                              <h3 class="widget-title text-dark">
                            Popular tags
                         </h3>
                              <div class="clearfix"></div>
                          </div>
                          <div class="widget-body">
                              <ul class="tags">
                                  <li> <a href="#" class="tag">
                               Coupons
                               </a> </li>
                                  <li> <a href="#" class="tag">
                               Discounts
                               </a> </li>
                                  <li> <a href="#" class="tag">
                               Deals
                               </a> </li>
                                  <li> <a href="#" class="tag">
                               Store
                               </a> </li>
                                  <li> <a href="#" class="tag">
                               Ebay
                               </a> </li>
                                  <li> <a href="#" class="tag">
                               Fashion
                               </a> </li>
                                  <li> <a href="#" class="tag">
                               Shoes
                               </a> </li>
                                  <li> <a href="#" class="tag">
                               Kids
                               </a> </li>
                                  <li> <a href="#" class="tag">
                               Travel
                               </a> </li>
                                  <li> <a href="#" class="tag">
                               Vacations
                               </a> </li>
                                  <li> <a href="#" class="tag">
                               Hosting
                               </a> </li>
                              </ul>
                          </div>
                      </div> --}}
                      <!-- // tags -->


                      {{-- <div class="widget">
                          <!-- /widget heading -->
                          <div class="widget-heading">
                              <h3 class="widget-title text-dark">
                            Top Stores
                         </h3>
                              <div class="widget-widgets"> <a href="#">View More Stores <span class="ti-angle-right"></span></a> </div>
                              <div class="clearfix"></div>
                          </div>
                          <div class="widget-body">
                              <div class="row">
                                  <div class="col-lg-6 col-md-2 col-sm-4 col-xs-6 thumb">
                                      <div class="thumb-inside">
                                          <a class="thumbnail" href="#"> <img class="img-responsive" src="assets/images/thumb6.png" alt=""> </a> <span class="favorite"><a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store"><i class="ti-heart"></i></a></span> </div>
                                      <div class="store_name text-center">
                                          <h5>Wallgrins</h5> </div>
                                  </div>
                                  <!-- /thumb -->
                                  <div class="col-lg-6 col-md-2 col-sm-4 col-xs-6 thumb">
                                      <div class="thumb-inside">
                                          <a class="thumbnail" href="#"> <img class="img-responsive" src="assets/images/thumb1.png" alt=""> </a> <span class="favorite"><a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store"><i class="ti-heart"></i></a></span> </div>
                                      <div class="store_name text-center">
                                          <h5>Onlinestore</h5> </div>
                                  </div>
                                  <!-- /thumb -->
                                  <div class="col-lg-6 col-md-2 col-sm-4 col-xs-6 thumb">
                                      <div class="thumb-inside">
                                          <a class="thumbnail" href="#"> <img class="img-responsive" src="assets/images/thumb2.png" alt=""> </a> <span class="favorite"><a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store"><i class="ti-heart"></i></a></span> </div>
                                      <div class="store_name text-center">
                                          <h5>Chegg</h5> </div>
                                  </div>
                                  <!-- /thumb -->
                                  <div class="col-lg-6 col-md-2 col-sm-4 col-xs-6 thumb">
                                      <div class="thumb-inside">
                                          <a class="thumbnail" href="#"> <img class="img-responsive" src="assets/images/thumb3.png" alt=""> </a> <span class="favorite"><a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store"><i class="ti-heart"></i></a></span> </div>
                                      <div class="store_name text-center">
                                          <h5>Shopname</h5> </div>
                                  </div>
                                  <!-- /thumb -->
                                  <div class="col-lg-6 col-md-2 col-sm-4 col-xs-6 thumb">
                                      <div class="thumb-inside">
                                          <a class="thumbnail" href="#"> <img class="img-responsive" src="assets/images/thumb4.png" alt=""> </a> <span class="favorite"><a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store"><i class="ti-heart"></i></a></span> </div>
                                      <div class="store_name text-center">
                                          <h5>Storename</h5> </div>
                                  </div>
                                  <!-- /thumb -->
                                  <div class="col-lg-6 col-md-2 col-sm-4 col-xs-6 thumb">
                                      <div class="thumb-inside">
                                          <a class="thumbnail" href="#"> <img class="img-responsive" src="assets/images/thumb5.png" alt=""> </a> <span class="favorite"><a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store"><i class="ti-heart"></i></a></span> </div>
                                      <div class="store_name text-center">
                                          <h5>VendorStore</h5> </div>
                                  </div>
                                  <!-- /thumb -->
                              </div>
                          </div>
                      </div> --}}
                  </div>
                  <!--/col -->
                  <div class="col-sm-9">
                      @foreach ($list as $coupon)
                        <div class="coupon-wrapper coupon-single">
                            <div class="row">
                                {{-- <div class="ribbon-wrapper hidden-xs">
                                    <div class="ribbon">Featured</div>
                                </div> --}}
                                <div class="coupon-data col-sm-2 text-center">
                                    <div class="savings text-center">
                                        <div>
                                            <div class="large">{{$coupon->discount}}%</div>
                                            <div class="small">off</div>
                                            <div class="type">Coupon</div>
                                        </div>
                                    </div>
                                    <!-- end:Savings -->
                                </div>
                                <!-- end:Coupon data -->
                                <div class="coupon-contain col-sm-7">
                                    <ul class="list-inline list-unstyled">
                                        {{-- <li class="sale label label-pink">Sale</li>
                                        <li class="popular label label-success">98% success</li> --}}
                                        {{-- <li><span class="verified  text-success"><i class="ti-face-smile"></i>Verified</span> </li> --}}
                                        <li><span class="used-count">{{$coupon->used}} used</span> </li>
                                    </ul>
                                    @php
                                      $find_store = Store::find($coupon->store);
                                    @endphp
                                    <h4 class="coupon-title"><a href="#">{{$find_store->name}}|| {{$coupon->category}} || {{$coupon->title}}</a></h4>
                                    <p data-toggle="collapse" data-target="#more">{{$coupon->description}}</p>
                                    <p id="more" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                    <ul class="coupon-details list-inline">
                                        {{-- <li class="list-inline-item">
                                            <div class="btn-group" role="group" aria-label="...">
                                                <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                                <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                            </div>
                                            <!-- end:Btn group -->
                                        </li>
                                        <li class="list-inline-item">30% of 54 recommend</li> --}}
                                        <li class="list-inline-item"><a href="#">Share</a> </li>
                                    </ul>
                                    <!-- end:Coupon details -->
                                </div>
                                <!-- end:Coupon cont -->
                                @php
                                  $valitTill = Carbon\Carbon::createFromFormat('Y-m-d', $coupon->valid_to)->format('d-m-Y');
                                @endphp
                                <div class="button-contain col-sm-3 text-center">
                                    <p class="btn-code" data-toggle="modal"> <span class="partial-code">Valid Till - {{$valitTill}}</span>  </p>
                                    {{-- <div class="btn-group" role="group" aria-label="...">
                                        <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                        <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                        <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                    </div> --}}
                                </div>
                            </div>
                            <!-- //row -->
                        </div>
                      @endforeach


                      <ul class="pagination pagination-lg ">
                          {{-- <li>
                              <a href="#"> <i class="ti-arrow-left"></i> </a>
                          </li>
                          <li> <a href="#">1</a> </li>
                          <li class="active"> <a href="#">2</a> </li>
                          <li> <a href="#">3</a> </li>
                          <li> <a href="#">4</a> </li>
                          <li>
                              <a href="#"> <i class="ti-arrow-right"></i> </a>
                          </li> --}}
                      </ul>
                  </div>
              </div>


               <!-- End row -->
            </div>



            <!-- Footer -->
            @include('backend.includes.mainfooter')
            <!-- start modal -->
           <!-- Large modal -->

           @php
             $coupons_list = Coupon::all();
           @endphp
           @foreach ($coupons_list as $coupon)
             <div class="coupon_modal modal fade couponModal{{$coupon->id}}" tabindex="-1" role="dialog" style="z-index: 99999">
                <div class="modal-dialog modal-lg" role="document">
                   <div class="modal-content">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ti-close"></i></span> </button>
                      <div class="coupon_modal_content">

                         <div class="row">
                            <div class="col-sm-10 col-sm-offset-1 text-center">
                               <h2>{{$coupon->title}}</h2>
                               <p>Not applicable to taxes, transfers,or gift cards. Cannot be used in conjunction with any other offer, sale, discount or promotion. After the initial purchase term.</p>
                            </div>

                         <div class="row">
                             <div class="col-sm-12">
                                 <h5 class="text-center text-uppercase m-t-20 text-muted">Click below to get your coupon code</h5>
                             </div>

                             @if (Auth::user())
                               <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                                 <a href="#" target="_blank" class="coupon_code alert alert-info">
                                   <span class="coupon_icon"><i class="ti-cut hidden-xs"></i></span>
                                   {{$coupon->coupon}}
                                 </a>
                               </div>
                               <div class="col-sm-12">
                                  <div class="newsletter-form">
                                     <h4><i class="ti-mobile"></i>Send Coupoun code to mobile.</h4>
                                     <div class="input-group">
                                        <input type="hidden" name="coupon" value="{{$coupon->coupon}}">

                                        <input type="text" class="form-control input-lg" placeholder="Mobile Number" id="couponId{{$coupon->id}}"> <span class="input-group-btn">
                                        <button class="btn btn-danger btn-lg" onclick="sendMessage({{$coupon->id}}, '{{$coupon->coupon}}', {{Auth::user()->id}})" type="button">
                                          Send
                                        </button>
                                        </span>
                                     </div>
                                     <p><small>We’ll never share your Mobile Number with a third-party.</small> </p>
                                  </div>
                               </div>
                               @else
                                 <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                                   <a href="/login">Please <strong> <u> Login </u></strong>to get Code..</a>
                                 </div>
                             @endif
                         </div>
                            <div class="row">
                               {{-- <div class="col-sm-12">
                                  <div class="report">Did this coupon work? <span class="yes vote-link" data-src="#">Yes</span> <span class="no vote-link" data-src="#">No</span> </div>
                               </div> --}}
                            </div>
                         </div>
                      </div>
                      <!-- end: Coupon modal content -->
                   </div>



                        {{-- <div class="newsletter-modal">
                         <div class="newsletter-form">
                            <h4><i class="ti-email"></i>Sign up for our weekly email newsletter with the best money-saving coupons.</h4>
                            <div class="input-group">
                               <input class="form-control input-lg" placeholder="Email" type="text"> <span class="input-group-btn">
                               <button class="btn btn-danger btn-lg" type="button">
                               Subscribe
                               </button>
                               </span>
                            </div>
                            <p><small>We’ll never share your email address with a third-party.</small> </p>
                            </div>
                        </div> --}}
                         <ul class="nav nav-pills nav-justified">
                      <li role="presentation" class="active"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It worked"><i class="ti-check color-green"></i></a> </li>
                      <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="I love it"><i class="ti-heart color-primary"></i></a> </li>
                      <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-close"></i></a> </li>
                   </ul>


                </div>
             </div>
           @endforeach

         <!-- end: Modall -->
         </div>
      </div>
      <!-- //wrapper -->
      <!-- jQuery  -->
      <script src="{{config('APP.URL')}}/assets/js/jquery.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/bootstrap.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/animsition.min.js"></script>
      <script src="{{config('APP.URL')}}/owl.carousel/owl.carousel.min.js"></script>
      <!-- Kupon js -->
      <script src="{{config('APP.URL')}}/assets/js/kupon.js"></script>
      <script type="text/javascript">
      function sendMessage(couponId, couponCode, userId) {
        // console.log(storeId);
        // console.log(userId);
        var userNumber = $('#couponId' + couponId).val()
        console.log(userNumber);
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        data = {_token: CSRF_TOKEN, userNumber: userNumber, couponId: couponId, couponCode:couponCode, userId: userId};
        $.ajax({
          url: '/my-coupoun',
          type: 'POST',
          data: data,
          dataType: 'JSON',
          success: function (data) {
            console.log(data);
            // if (data.status == 'ok') {
            //   console.log('success');
            //   // $('#storeId' + storeId).attr('style', 'color: red');
            // }
          }
        });
      }
      </script>
   </body>

<!-- Mirrored from codenpixel.com/demo/kuponhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:54:20 GMT -->
</html>

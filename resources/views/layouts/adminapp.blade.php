<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<meta charset="utf-8"/>
<title>{{ config('app.name', 'Laravel') }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok3v=1613a3a185/"},atok:"bae3d44d6af79fe327d292f365c8de21",petok:"40c57ecf6295a97e161cc4d910aa5f283906af17-1492867514-1800",zone:"revox.io",rocket:"0",apps:{"ga_key":{"ua":"UA-56895490-1","ga_bs":"1"}}}];!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="//ajax.cloudflare.com/cdn-cgi/nexp/dok3v=85b614c0f6/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
//]]>
</script>
<link rel="apple-touch-icon" href="{{config('app.url')}}/pages/ico/60.png">
<link rel="apple-touch-icon" sizes="76x76" href="{{config('app.url')}}/pages/ico/76.png">
<link rel="apple-touch-icon" sizes="120x120" href="{{config('app.url')}}/pages/ico/120.png">
<link rel="apple-touch-icon" sizes="152x152" href="{{config('app.url')}}/pages/ico/152.png">
<link rel="icon" type="image/x-icon" href="favicon.ico"/>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
<link href="{{config('APP_URL')}}/backend/assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('APP_URL')}}/backend/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
<link href="{{config('APP_URL')}}/backend/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css"/>
<link href="{{config('APP_URL')}}/backend/assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen"/>
<link class="main-stylesheet" href="{{config('APP_URL')}}/backend/pages/css/pages.css" rel="stylesheet" type="text/css"/>
<!--[if lte IE 9]>
	<link href="{{config('APP_URL')}}/backend/assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
	<![endif]-->
<script type="text/javascript">
/* <![CDATA[ */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-56895490-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

/* ]]> */
</script>
</head>
<body class="fixed-header dashboard">

@include('backend.includes.sidebar')


<div class="page-container">

@include('backend.includes.navbar')

<div class="page-content-wrapper">

<div class="content sm-gutter">

<div class="container-fluid padding-25 sm-padding-10">

  @yield('content')

</div>

</div>


@include('backend.includes.footer')


</div>

</div>

<script src="{{config('APP_URL')}}/backend/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="{{config('APP_URL')}}/backend/assets/plugins/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="{{config('APP_URL')}}/backend/assets/plugins/classie/classie.js"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/nvd3/lib/d3.v3.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/nvd3/nv.d3.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/nvd3/src/utils.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/nvd3/src/tooltip.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/nvd3/src/interactiveLayer.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/nvd3/src/models/axis.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/nvd3/src/models/line.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/nvd3/src/models/lineWithFocusChart.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/mapplic/js/hammer.js"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/mapplic/js/jquery.mousewheel.js"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/mapplic/js/mapplic.js"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/rickshaw/rickshaw.min.js"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-metrojs/MetroJs.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/skycons/skycons.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
<script type="text/javascript" src="{{config('APP_URL')}}/backend/assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="{{config('APP_URL')}}/backend/assets/plugins/datatables-responsive/js/lodash.min.js"></script>

<script src="{{config('APP_URL')}}/backend/pages/js/pages.min.js"></script>


<script src="{{config('APP_URL')}}/backend/assets/js/dashboard.js" type="text/javascript"></script>
<script src="{{config('APP_URL')}}/backend/assets/js/scripts.js" type="text/javascript"></script>

<script src="{{config('APP_URL')}}/backend/assets/js/demo.js" type="text/javascript"></script>
<script>
		 window.intercomSettings = {
		   app_id: "xt5z6ibr"
		 };
		</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/xt5z6ibr';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
</body>
</html>

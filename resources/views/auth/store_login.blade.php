<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<meta charset="utf-8"/>
<title>{{config('app.name')}}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok3v=1613a3a185/"},atok:"bae3d44d6af79fe327d292f365c8de21",petok:"3c8f6277417955e89d7870443c41954e36395c45-1492867836-1800",zone:"revox.io",rocket:"0",apps:{"ga_key":{"ua":"UA-56895490-1","ga_bs":"1"}}}];!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="//ajax.cloudflare.com/cdn-cgi/nexp/dok3v=85b614c0f6/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
//]]>
</script>
<link rel="apple-touch-icon" href="{{config('app.url')}}/backend/pages/ico/60.png">
<link rel="apple-touch-icon" sizes="76x76" href="{{config('app.url')}}/backend/pages/ico/76.png">
<link rel="apple-touch-icon" sizes="120x120" href="{{config('app.url')}}/backend/pages/ico/120.png">
<link rel="apple-touch-icon" sizes="152x152" href="{{config('app.url')}}/backend/pages/ico/152.png">
<link rel="icon" type="image/x-icon" href="favicon.ico"/>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="{{config('app.url')}}/backend/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('app.url')}}/backend/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('app.url')}}/backend/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('app.url')}}/backend/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{config('app.url')}}/backend/pages/css/pages.css" rel="stylesheet" type="text/css"/>
<!--[if lte IE 9]>
        <link href="{{config('app.url')}}/backend/pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
<script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{config('app.url')}}/backend/pages/css/windows.chrome.fix.css" />'
    }
    </script>
<script type="text/javascript">
/* <![CDATA[ */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-56895490-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

/* ]]> */
</script>
</head>
<body class="fixed-header">
<div class="login-wrapper">

<div class="bg-pic">

<img src="{{config('app.url')}}/backend/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src="{{config('app.url')}}/backend/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src-retina="{{config('app.url')}}/backend/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" alt="" class="lazy">


<div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
<h2 class="semi-bold text-white">Admin</h2>
<p class="small">
images Displayed are solely for representation purposes only, All work copyright of respective
owner, otherwise © 2018 vJsOFT
</p>
</div>

</div>


<div class="login-container bg-white">
<div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
<img src="{{config('app.url')}}/backend/assets/img/logo.png" alt="logo" data-src="{{config('app.url')}}/backend/assets/img/logo.png" data-src-retina="{{config('app.url')}}/backend/assets/img/logo_2x.png" width="78" height="22">
<p class="p-t-35">Sign into your pages account</p>

<form id="form-login" class="p-t-15" method="POST" action="{{ route('validation.login.submit') }}">
  {{ csrf_field() }}
<div class="form-group form-group-default {{ $errors->has('name') ? ' has-error' : '' }}">
<label>Login</label>
<div class="controls">
<input type="text" name="email" placeholder="Email" class="form-control" required value="{{ old('email') }}" autofocus>

      @if ($errors->has('email'))
          <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
      @endif
</div>
</div>


<div class="form-group form-group-default {{ $errors->has('password') ? ' has-error' : '' }}">
<label>Password</label>
<div class="controls">
<input type="password" class="form-control" name="password" placeholder="Credentials" required>
@if ($errors->has('password'))
    <span class="help-block">
        <strong>{{ $errors->first('password') }}</strong>
    </span>
@endif
</div>
</div>

<div class="row">
<div class="col-md-6 no-padding">
{{-- <div class="checkbox">
<a href="{{ route('register') }}" class="text-info small"> Create Account</a>
</div> --}}
</div>
<div class="col-md-6 text-right">
<a href="{{ route('password.request') }}" class="text-info small"> Forgot Your Password?</a>
</div>
</div>

<button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
</form>


</div>
</div>

</div>

<div class="overlay hide" data-pages="search">

<div class="overlay-content has-results m-t-20">

<div class="container-fluid">

<img class="overlay-brand" src="{{config('app.url')}}/backend/assets/img/logo.png" alt="logo" data-src="{{config('app.url')}}/backend/assets/img/logo.png" data-src-retina="{{config('app.url')}}/backend/assets/img/logo_2x.png" width="78" height="22">


<a href="#" class="close-icon-light overlay-close text-black fs-16">
<i class="pg-close"></i>
</a>

</div>

<div class="container-fluid">

<input id="overlay-search" class="no-border overlay-search bg-transparent" placeholder="Search..." autocomplete="off" spellcheck="false">
<br>
<div class="inline-block">
<div class="checkbox right">
<input id="checkboxn" type="checkbox" value="1" checked="checked">
<label for="checkboxn"><i class="fa fa-search"></i> Search within page</label>
</div>
</div>
<div class="inline-block m-l-10">
<p class="fs-13">Press enter to search</p>
</div>

</div>

<div class="container-fluid">
<span>
<strong>suggestions :</strong>
</span>
<span id="overlay-suggestions"></span>
<br>
<div class="search-results m-t-40">
<p class="bold">Pages Search Results</p>
<div class="row">
<div class="col-md-6">

<div class="">

<div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
<div>
<img width="50" height="50" src="{{config('app.url')}}/backend/assets/img/profiles/avatar.jpg" data-src="{{config('app.url')}}/backend/assets/img/profiles/avatar.jpg" data-src-retina="{{config('app.url')}}/backend/assets/img/profiles/avatar2x.jpg" alt="">
</div>
</div>

<div class="p-l-10 inline p-t-5">
<h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> on pages</h5>
<p class="hint-text">via john smith</p>
</div>
</div>


<div class="">

<div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
<div>T</div>
</div>

<div class="p-l-10 inline p-t-5">
<h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> related topics</h5>
<p class="hint-text">via pages</p>
</div>
</div>


<div class="">

<div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10">
<div><i class="fa fa-headphones large-text"></i>
</div>
</div>

<div class="p-l-10 inline p-t-5">
<h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> music</h5>
<p class="hint-text">via pagesmix</p>
</div>
</div>

</div>
<div class="col-md-6">

<div class="">

<div class="thumbnail-wrapper d48 circular bg-info text-white inline m-t-10">
<div><i class="fa fa-facebook large-text"></i>
</div>
</div>

<div class="p-l-10 inline p-t-5">
<h5 class="m-b-5"><span class="semi-bold result-name">ice cream</span> on facebook</h5>
<p class="hint-text">via facebook</p>
</div>
</div>


<div class="">

<div class="thumbnail-wrapper d48 circular bg-complete text-white inline m-t-10">
<div><i class="fa fa-twitter large-text"></i>
</div>
</div>

<div class="p-l-10 inline p-t-5">
<h5 class="m-b-5">Tweats on<span class="semi-bold result-name"> ice cream</span></h5>
<p class="hint-text">via twitter</p>
</div>
</div>


<div class="">

<div class="thumbnail-wrapper d48 circular text-white bg-danger inline m-t-10">
<div><i class="fa fa-google-plus large-text"></i>
</div>
</div>

<div class="p-l-10 inline p-t-5">
<h5 class="m-b-5">Circles on<span class="semi-bold result-name"> ice cream</span></h5>
<p class="hint-text">via google plus</p>
</div>
</div>

</div>
</div>
</div>
</div>

</div>

</div>


<script src="{{config('app.url')}}/backend/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/classie/classie.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>

<script src="{{config('app.url')}}/backend/pages/js/pages.min.js"></script>
<script>
    $(function()
    {
      $('#form-login').validate()
    })
    </script>
</body>
</html>

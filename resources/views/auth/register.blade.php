@php
  use App\Coupon;
  use App\Shop;
  use App\Category;
@endphp
  <!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codenpixel.com/demo/kuponhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:53:44 GMT -->
<head>
      <meta charset="utf-8" />
      <title>Alchal Registration</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta content="" name="description" />
      <meta content="Kupons" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="#">
      <link href="{{config('APP.URL')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animate.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animsition.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css">
      <!-- Theme styles -->
      <link href="{{config('APP.URL')}}/assets/css/style.css" rel="stylesheet" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!-- Navigation Bar-->
          @include('backend.includes.header')
         <!-- Navigation ends -->
         <div class="wrapper">
            <div class="container">
               <div class="row">
                  <br>
                  <!-- /col 12 -->
               </div>

               <div class="row">
                  <div class="col-lg-3">
                  </div>
                  <div class="col-lg-6">

                     <div class="widget">
                        <!-- /widget heading -->
                        <div class="widget-heading">
                           <h3 class="widget-title text-dark">
                              Register
                           </h3>
                           <div class="clearfix"></div>
                        </div>
                        <div class="widget-body">
                           <form class="form-horizontal select-search" method="POST" method="POST" action="{{ route('register') }}">
                               {{ csrf_field() }}
                              <fieldset>
                                 <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="control-label ">Name</label>
                                    <input class="form-control" id="name" type="text"  name="name" value="{{ old('name') }}" required autofocus />
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                 </div>
                                 <div class="form-group {{ $errors->has('mobile') ? ' has-error' : '' }}">
                                    <label class="control-label ">Mobile</label>
                                    <input class="form-control" id="mobile" type="text" max-length="10"  name="mobile" value="{{ old('mobile') }}" required  />
                                    @if ($errors->has('mobile'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('mobile') }}</strong>
                                        </span>
                                    @endif
                                 </div>
                                 <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="control-label ">Email</label>
                                    <input class="form-control" id="email" type="email"  name="email" value="{{ old('email') }}" required autofocus />
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                 </div>
                                 <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="control-label ">Password</label>
                                    <input class="form-control" id="password" type="password"  name="password" required/>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label ">Confirm Password</label>
                                    <input class="form-control" id="password-confirm" type="password" name="password_confirmation" required/>

                                 </div>

                                 <!-- //row -->
                                 <!-- Button -->
                                 <div class="form-group ">
                                   <div class="col-md-6">
                                     <button type="submit"  class="btn btn-danger">
                                       Register
                                     </button>
                                     <a href="login/facebook" class="btn btn-primary">
                                       Facebook
                                     </a>
                                   </div>
                                   <div class="col-md-6">
                                     <a class="btn btn-link" href="{{ route('login') }}">
                                         Click here for Login
                                     </a>
                                   </div>
                                    {{-- <button id="search_btn" name="search_btn" class="btn btn-danger">Search coupons</button> --}}
                                 </div>
                              </fieldset>
                           </form>
                        </div>
                     </div>
                     <!-- /widget -->


                     <!-- /widget -->

                     <!-- /widget -->
                  </div>
                  <!-- end col -->
               </div>
               <!-- End row -->
            </div>

            <section class="newsletter-alert">
               {{-- <div class="container text-center">
                  <div class="col-sm-12">
                     <div class="newsletter-form">
                        <h4><i class="ti-email"></i>Sign up for our weekly email newsletter with the best money-saving coupons.</h4>
                        <div class="input-group">
                           <input type="text" class="form-control input-lg" placeholder="Email"> <span class="input-group-btn">
                           <button class="btn btn-danger btn-lg" type="button">
                           Subscribe
                           </button>
                           </span>
                        </div>
                        <p><small>We’ll never share your email address with a third-party.</small> </p>
                     </div>
                  </div>
               </div> --}}
            </section>
            <!-- end:Newsletter signup -->
            <!-- Footer -->
              @include('backend.includes.mainfooter')
            <!-- start modal -->
           <!-- Large modal -->
           @php

           @endphp

           @php
             $coupons_list = Coupon::all();
           @endphp
           @foreach ($coupons_list as $coupon)
             <div class="coupon_modal modal fade couponModal{{$coupon->id}}" tabindex="-1" role="dialog" style="z-index: 99999">
                <div class="modal-dialog modal-lg" role="document">
                   <div class="modal-content">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ti-close"></i></span> </button>
                      <div class="coupon_modal_content">

                         <div class="row">
                            <div class="col-sm-10 col-sm-offset-1 text-center">
                               <h2>{{$coupon->title}}</h2>
                               <p>Not applicable to ICANN fees, taxes, transfers,or gift cards. Cannot be used in conjunction with any other offer, sale, discount or promotion. After the initial purchase term.</p>
                            </div>

                         <div class="row">
                             <div class="col-sm-12">
                                 <h5 class="text-center text-uppercase m-t-20 text-muted">Click below to get your coupon code</h5>
                             </div>

                            <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                              <a href="#" target="_blank" class="coupon_code alert alert-info">
                                <span class="coupon_icon"><i class="ti-cut hidden-xs"></i></span>  {{$coupon->coupon}}
                               </a>
                            </div>
                         </div>
                            <div class="row">
                               <div class="col-sm-12">
                                  <div class="report">Did this coupon work? <span class="yes vote-link" data-src="#">Yes</span> <span class="no vote-link" data-src="#">No</span> </div>
                               </div>
                            </div>
                         </div>
                      </div>
                      <!-- end: Coupon modal content -->
                   </div>



                        <div class="newsletter-modal">
                         <div class="newsletter-form">
                            <h4><i class="ti-email"></i>Sign up for our weekly email newsletter with the best money-saving coupons.</h4>
                            <div class="input-group">
                               <input class="form-control input-lg" placeholder="Email" type="text"> <span class="input-group-btn">
                               <button class="btn btn-danger btn-lg" type="button">
                               Subscribe
                               </button>
                               </span>
                            </div>
                            <p><small>We’ll never share your email address with a third-party.</small> </p>
                            </div>
                        </div>
                         <ul class="nav nav-pills nav-justified">
                      <li role="presentation" class="active"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It worked"><i class="ti-check color-green"></i></a> </li>
                      <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="I love it"><i class="ti-heart color-primary"></i></a> </li>
                      <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-close"></i></a> </li>
                   </ul>


                </div>
             </div>
           @endforeach

         <!-- end: Modall -->
         </div>
      </div>
      <!-- //wrapper -->
      <!-- jQuery  -->
      <script src="{{config('APP.URL')}}/assets/js/jquery.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/bootstrap.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/animsition.min.js"></script>
      <script src="{{config('APP.URL')}}/owl.carousel/owl.carousel.min.js"></script>
      <!-- Kupon js -->
      <script src="{{config('APP.URL')}}/assets/js/kupon.js"></script>
   </body>

<!-- Mirrored from codenpixel.com/demo/kuponhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:54:20 GMT -->
</html>

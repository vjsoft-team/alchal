@php
  use App\Shop;
  use App\Category;
@endphp
<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<meta charset="utf-8"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ config('app.name', 'Laravel') }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok3v=1613a3a185/"},atok:"bae3d44d6af79fe327d292f365c8de21",petok:"40c57ecf6295a97e161cc4d910aa5f283906af17-1492867514-1800",zone:"revox.io",rocket:"0",apps:{"ga_key":{"ua":"UA-56895490-1","ga_bs":"1"}}}];!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="//ajax.cloudflare.com/cdn-cgi/nexp/dok3v=85b614c0f6/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
//]]>
</script>
<link rel="apple-touch-icon" href="{{config('app.url')}}/backend/pages/ico/60.png">
<link rel="apple-touch-icon" sizes="76x76" href="{{config('app.url')}}/backend/pages/ico/76.png">
<link rel="apple-touch-icon" sizes="120x120" href="{{config('app.url')}}/backend/pages/ico/120.png">
<link rel="apple-touch-icon" sizes="152x152" href="{{config('app.url')}}/backend/pages/ico/152.png">
<link rel="icon" type="image/x-icon" href="favicon.ico"/>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="{{config('app.url')}}/backend/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('app.url')}}/backend/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('app.url')}}/backend/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrap-tag/bootstrap-tagsinput.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
<link href="{{config('app.url')}}/backend/assets/plugins/summernote/css/summernote.css" rel="stylesheet" type="text/css" media="screen">
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" media="screen">
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" media="screen">
<link href="{{config('app.url')}}/backend/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{config('app.url')}}/backend/pages/css/pages.css" rel="stylesheet" type="text/css"/>
<!--[if lte IE 9]>
	<link href="{{config('APP_URL')}}/assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
	<![endif]-->
<script type="text/javascript">
/* <![CDATA[ */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-56895490-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

/* ]]> */
</script>
</head>
<body class="fixed-header dashboard">

@include('backend.includes.sidebar')


<div class="page-container">

@include('backend.includes.navbar')

<div class="page-content-wrapper">

<div class="content sm-gutter">

<div class="container-fluid padding-25 sm-padding-10">

  <div class="panel panel-default">
  <div class="panel-heading">
  <div class="panel-title">
  Edit Coupons
  </div>
  </div>
  <div class="panel-body">
{!! Form::open(['action' => ['CouponController@update',$key->id], 'method'=> 'POST']) !!}
      <div class="row">
            <div class="col-sm-3">
              <div class="form-group form-group-default form-group-default-select2 required">
              <label>Shop Name</label>
              <select class="form-control" name="store" data-init-plugin="select2">
                <option value="">Select</option>
                @php
                  $shops_list = Shop::all();
                @endphp
                @foreach ($shops_list as $shops)
                  <option @if ($shops->name == $key->store)
                    selected
                  @endif value="{{$shops->name}}">{{$shops->name}}</option>
                @endforeach
                @if ($errors->has('store'))
                    <span class="help-block">
                        <strong>{{ $errors->first('store') }}</strong>
                    </span>
                @endif
              </select>
              </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group form-group-default form-group-default-select2 required">
            <label>Category Name</label>
            <select class="form-control" name="category" data-init-plugin="select2" >
              <option value="">Select</option>
              @php
                $category_list = Category::all();
              @endphp
              @foreach ($category_list as $category)
                <option @if ($category->category == $key->category)
                  selected
                @endif value="{{$category->category}}">{{$category->category}}</option>
              @endforeach
            </select>
            @if ($errors->has('category'))
                <span class="help-block">
                    <strong>{{ $errors->first('category') }}</strong>
                </span>
            @endif
            </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group form-group-default required">
            <label>Coupon</label>
            <input type="text" name="coupon" class="form-control" required value="{{$key->coupon}}">
            @if ($errors->has('coupon'))
                <span class="help-block">
                    <strong>{{ $errors->first('coupon') }}</strong>
                </span>
            @endif
            </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group form-group-default required">
            <label>Title</label>
            <input type="text" name="title" class="form-control" required value="{{$key->description}}">
            @if ($errors->has('title'))
                <span class="help-block">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
            </div>
            </div>


          </div>
          <div class="row">
            <div class="col-sm-3">
            <div class="form-group form-group-default required">
            <label>Description</label>
            <input type="text" name="description" class="form-control" required value="{{$key->description}}">
            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
            </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group form-group-default required">
            <label>Discount %</label>
            <input type="text" name="discount" class="form-control" required onkeypress='validate(event)' value="{{$key->discount}}">
            @if ($errors->has('discount'))
                <span class="help-block">
                    <strong>{{ $errors->first('discount') }}</strong>
                </span>
            @endif
            </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group form-group-default input-group required">
            <label>Valid From</label>
            <input type="text" name="valid_from" class="form-control" id="datepicker-component2" data-date-format="yyyy/mm/dd" required value="{{$key->valid_from}}">
            <span class="input-group-addon">
            <i class="fa fa-calendar"></i>
            </span>
            </div>
            </div>

              <div class="col-sm-3">
              <div class="form-group form-group-default input-group required">
              <label>Valid To</label>
              <input type="text" name="valid_to" class="form-control"  id="datepicker-component2" data-date-format="yyyy/mm/dd" required value="{{$key->valid_to}}">
              <span class="input-group-addon">
              <i class="fa fa-calendar"></i>
              </span>
              </div>
            </div>

                {{Form::hidden('_method','PUT')}}
              <input type="submit" name="submit" value="submit" class="btn btn-success pull-right">


          </div>
{!! Form::close() !!}
  </div>

</div>

</div>

@include('backend.includes.footer')
</div>

</div>



<script src="{{config('app.url')}}/backend/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/classie/classie.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/jquery-autonumeric/autoNumeric.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/moment/moment.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-typehead/typeahead.bundle.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-typehead/typeahead.jquery.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/handlebars/handlebars-v4.0.5.js"></script>


<script src="{{config('app.url')}}/backend/pages/js/pages.min.js"></script>


<script src="{{config('app.url')}}/backend/assets/js/form_elements.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/js/scripts.js" type="text/javascript"></script>

<script src="{{config('app.url')}}/backend/assets/js/demo.js" type="text/javascript"></script>

<script type="text/javascript">
function validate(evt) {
var theEvent = evt || window.event;
var key = theEvent.keyCode || theEvent.which;
key = String.fromCharCode( key );
var regex = /[0-9]|\./;
if( !regex.test(key) ) {
  theEvent.returnValue = false;
  if(theEvent.preventDefault) theEvent.preventDefault();
}
}
</script>


<script>
		 window.intercomSettings = {
		   app_id: "xt5z6ibr"
		 };
		</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/xt5z6ibr';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
</body>
</html>

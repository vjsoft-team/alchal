@if (Auth::guard('web')->check())
  <p class="text-success">
    You Are Logged_in as a <strong>USER</strong>
  </p>
  @else
    <p class="text-danger">
      You Are Logged-Out as a <strong>USER</strong>
    </p>
@endif
@if (Auth::guard('store')->check())
  <p class="text-success">
    You Are Logged_in as a <strong>STORE</strong>
  </p>
  @else
    <p class="text-danger">
      You Are Logged-Out as a <strong>STORE</strong>
    </p>
@endif

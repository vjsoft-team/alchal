@php
  use App\Category;
@endphp
<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<meta charset="utf-8"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ config('app.name', 'Laravel') }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok3v=1613a3a185/"},atok:"bae3d44d6af79fe327d292f365c8de21",petok:"40c57ecf6295a97e161cc4d910aa5f283906af17-1492867514-1800",zone:"revox.io",rocket:"0",apps:{"ga_key":{"ua":"UA-56895490-1","ga_bs":"1"}}}];!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="//ajax.cloudflare.com/cdn-cgi/nexp/dok3v=85b614c0f6/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
//]]>
</script>
<link rel="apple-touch-icon" href="{{config('app.url')}}/backend/pages/ico/60.png">
<link rel="apple-touch-icon" sizes="76x76" href="{{config('app.url')}}/backend/pages/ico/76.png">
<link rel="apple-touch-icon" sizes="120x120" href="{{config('app.url')}}/backend/pages/ico/120.png">
<link rel="apple-touch-icon" sizes="152x152" href="{{config('app.url')}}/backend/pages/ico/152.png">
<link rel="icon" type="image/x-icon" href="favicon.ico"/>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="{{config('app.url')}}/backend/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('app.url')}}/backend/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('app.url')}}/backend/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrap-tag/bootstrap-tagsinput.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css"/>
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
<link href="{{config('app.url')}}/backend/assets/plugins/summernote/css/summernote.css" rel="stylesheet" type="text/css" media="screen">
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" media="screen">
<link href="{{config('app.url')}}/backend/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" media="screen">
<link href="{{config('app.url')}}/backend/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{config('app.url')}}/backend/pages/css/pages.css" rel="stylesheet" type="text/css"/>
<!--[if lte IE 9]>
	<link href="{{config('APP_URL')}}/assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
	<![endif]-->
<script type="text/javascript">
/* <![CDATA[ */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-56895490-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

/* ]]> */
</script>
</head>
<body class="fixed-header dashboard">

@include('backend.includes.sidebar')


<div class="page-container">

@include('backend.includes.navbar')

<div class="page-content-wrapper">

<div class="content sm-gutter">

<div class="container-fluid padding-25 sm-padding-10">

  <div class="panel panel-default">
  <div class="panel-heading">
  <div class="panel-title">
  Add Shops
  </div>
  </div>
  <div class="panel-body">

  <form class="" action="{{action('ShopController@store')}}" method="POST" enctype="multipart/form-data">
  {{ csrf_field() }}
      <div class="row">
            <div class="col-sm-3">
            <div class="form-group form-group-default required">
            <label>Shop Name</label>
            <input type="text" name="name" class="form-control" required value="{{old('name')}}">
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
            </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group form-group-default">
            <label>Owner Name</label>
            <input type="text" name="owner_name" class="form-control" value="{{old('owner_name')}}">
            @if ($errors->has('owner_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('owner_name') }}</strong>
                </span>
              @endif
            </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group form-group-default ">
            <label>Owner Number</label>
            <input type="text" name="owner_no" class="form-control" maxlength="10"onkeypress='validate(event)' value="{{old('owner_no')}}">
            @if ($errors->has('owner_no'))
                <span class="help-block">
                    <strong>{{ $errors->first('owner_no') }}</strong>
                </span>
            @endif
            </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group form-group-default required">
            <label>Contact Person Name</label>
            <input type="text" name="contact_person_name" class="form-control" required value="{{old('contact_person_name')}}">
            @if ($errors->has('category_person_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('category_person_name') }}</strong>
                </span>
            @endif
            </div>
            </div>

          </div>
          <div class="row">
            <div class="col-sm-3">
            <div class="form-group form-group-default required">
            <label>Contact Person Number</label>
            <input type="text" name="contact_person_no" class="form-control" maxlength="10" required onkeypress='validate(event)' value="{{old('contact_person_no')}}">
            @if ($errors->has('category_person_no'))
                <span class="help-block">
                    <strong>{{ $errors->first('category_person_no') }}</strong>
                </span>
            @endif
            </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group form-group-default">
            <label>Shop Number</label>
            <input type="text" name="shop_mobile" class="form-control" maxlength="10" onkeypress='validate(event)' value="{{old('shop_mobile')}}">
            @if ($errors->has('shop_mobile'))
                <span class="help-block">
                    <strong>{{ $errors->first('shop_mobile') }}</strong>
                </span>
            @endif
            </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group form-group-default ">
            <label>Shop Landline</label>
            <input type="text" name="shop_landline" class="form-control"  onkeypress='validate(event)' value="{{old('shop_landline')}}">
            @if ($errors->has('shop_landline'))
                <span class="help-block">
                    <strong>{{ $errors->first('shop_landline') }}</strong>
                </span>
            @endif
            </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group form-group-default required">
            <label>Address</label>
            <textarea name="address" class="form-control"></textarea>
            </div>
            </div>

            </div>
          <div class="row">
            <div class="col-sm-3">
            <div class="form-group form-group-default">
            <label>Shop logo</label>
            <input type="file" name="shop_logo" class="form-control" >
            @if ($errors->has('shop_logo'))
                <span class="help-block">
                    <strong>{{ $errors->first('shop_logo') }}</strong>
                </span>
            @endif
            </div>
            </div>
            <div class="col-sm-2">
              <input type="submit" name="submit" value="submit" class="btn btn-success pull-right">
            </div>

            </div>


  </form>
  </div>
  </div>

</div>
<div class="container-fluid container-fixed-lg bg-white">

    <div class="panel panel-transparent">
          <div class="panel-heading">
              <div class="panel-title">Shops List
              </div>

              <div class="clearfix"></div>
          </div>
          <div class="panel-body">
              <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                  <thead>
                      <tr>
                          <th style="width:10%">Shop Name</th>
                          <th style="width:10%">Owner Name</th>
                          <th style="width:15%">owner Number</th>
                          <th style="width:15%">Contact Person</th>
                          <th style="width:15%">Contact Person Name</th>
                          <th style="width:15%">Shop Mobile</th>
                          <th style="width:15%">Landline</th>
                          <th style="width:15%">Address</th>
                          <th style="width:15%">Edit</th>
                          <th style="width:15%">Delete</th>

                      </tr>
                  </thead>
                  <tbody>
                      @if (count($list) > 0)
                        @foreach ($list as $key)
                      <tr>

                              <td>{{$key->name}}</td>
                              <td>{{$key->owner_name}}</td>
                              <td>{{$key->owner_no}}</td>
                              <td>{{$key->contact_person_name}}</td>
                              <td>{{$key->contact_person_no}}</td>
                              <td>{{$key->shop_mobile}}</td>
                              <td>{{$key->shop_landline}}</td>
                              <td>{{$key->address}}</td>
                              <td><a href="/disc/shops/{{$key->id}}/edit">Edit</a><br></td>
                              <td>
                                      {!! Form::open(['action' => ['ShopController@destroy',$key->id], 'method'=>'POST']) !!}
                                      {{Form::hidden('_method','delete')}}
                                      {{Form::submit('Delete')}}
                                      {!! Form::close() !!}
                              </td>
                      </tr>

                      @endforeach
                    @endif
                  </tbody>

              </table>
          </div>
      </div>

  </div>
</div>

@include('backend.includes.footer')
</div>

</div>



<script src="{{config('app.url')}}/backend/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/classie/classie.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/jquery-autonumeric/autoNumeric.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="{{config('app.url')}}/backend/assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/moment/moment.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-typehead/typeahead.bundle.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/bootstrap-typehead/typeahead.jquery.min.js"></script>
<script src="{{config('app.url')}}/backend/assets/plugins/handlebars/handlebars-v4.0.5.js"></script>


<script src="{{config('app.url')}}/backend/pages/js/pages.min.js"></script>


<script src="{{config('app.url')}}/backend/assets/js/form_elements.js" type="text/javascript"></script>
<script src="{{config('app.url')}}/backend/assets/js/scripts.js" type="text/javascript"></script>

<script src="{{config('app.url')}}/backend/assets/js/demo.js" type="text/javascript"></script>

<script type="text/javascript">
function validate(evt) {
var theEvent = evt || window.event;
var key = theEvent.keyCode || theEvent.which;
key = String.fromCharCode( key );
var regex = /[0-9]|\./;
if( !regex.test(key) ) {
  theEvent.returnValue = false;
  if(theEvent.preventDefault) theEvent.preventDefault();
}
}
</script>


<script>
		 window.intercomSettings = {
		   app_id: "xt5z6ibr"
		 };
		</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/xt5z6ibr';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
</body>
</html>

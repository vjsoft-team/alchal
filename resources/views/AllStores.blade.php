@php
  use App\Favouritestore;
  use App\Category;
  use App\Store;
  use App\Shop;
  $pageId = 3;
@endphp
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codenpixel.com/demo/kuponhub/results_2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:54:35 GMT -->
<head>
      <meta charset="utf-8" />
      <title>All Stores</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta content="" name="description" />
      <meta content="Kupons" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="#">
      <link href="{{config('APP.URL')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animate.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animsition.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css">
      <!-- Theme styles -->
      <link href="{{config('APP.URL')}}/assets/css/style.css" rel="stylesheet" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!-- Navigation Bar-->
         @include('backend.includes.header')
         <!-- Navigation ends -->

         <section class="results m-t-30">
            <div class="container">
               <div class="row">
                  <div class="col-sm-3">
                     <div class="widget">
                        <!-- /widget heading -->

                        <div class="widget-body">
                           <form class="form-horizontal select-search">
                              <label class="control-label">What you searching for?</label>
                              <div class="btn-group" data-toggle="buttons">
                                 <label class="btn btn-default active"> <i class="ti-tag"></i>
                                 <input type="checkbox" checked="">Coupons</label>
                                 <label class="btn btn-default"> <i class="ti-cut"></i>
                                 <input type="checkbox">Discounts</label>
                                 <label class="btn btn-default"> <i class="ti-alarm-clock"></i>
                                 <input type="checkbox">Deals</label>
                              </div>
                              <fieldset>
                                 <div class="form-group">
                                    <label class="control-label">Keyword</label>
                                    <input class="form-control" id="text" name="text" type="text">
                                 </div>
                                 <div class="row">
                                    <!-- Select Basic -->
                                    <div class="form-group col-sm-6 col-xs-12">
                                       <label class="control-label " for="category">Select category</label>
                                       <select class="select form-control" id="category" name="category">
                                         @php
                                           $catgories = Category::All();
                                         @endphp
                                         @foreach ($catgories as $categories)

                                           <option value="{{$categories->category}}">{{$categories->category}}</option>
                                         @endforeach

                                       </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-xs-12">
                                       <label class="control-label " for="store">Select a store</label>
                                       <select class="select form-control" id="store" name="store">
                                         @php
                                           $shop_list = Store::all();
                                         @endphp
                                         @foreach ($shop_list as $value1)

                                           <option value="{{$value1->name}}">{{$value1->name}}</option>
                                         @endforeach

                                       </select>
                                    </div>
                                 </div>
                                 <!-- //row -->
                                 <!-- Button -->
                                 <div class="form-group ">
                                    <button id="search_btn" name="search_btn" class="btn btn-danger">Search coupons</button>
                                 </div>
                              </fieldset>
                           </form>
                        </div>
                     </div>


                     <!-- // tags -->
                  </div>
                  <!--/col -->
                  <div class="col-sm-9">
                    <div class="widget-body">
                       <div class="widget">
                          <ul class="nav nav-tabs solo-nav responsive-tabs" id="myTab">
                            <li class="active"><a data-toggle="tab" href="#popular"><i class="ti-receipt"></i>All Stores <span class="badge badge-info">{{count($list)}}</span> </a> </li>
                            @php
                                if (Auth::user()){
                                  $uid = Auth::user()->id;
                                  $favourites = Favouritestore::where('user_id', $uid)->get();
                                }
                              @endphp
                              @if (Auth::user())

                                <li class=""><a data-toggle="tab" href="#inStore"><i class="ti-bar-chart"></i>Favorite Stores <span class="badge badge-purple">{{count($favourites)}}</span></a> </li>
                              @endif
                         {{--  <li class=""><a data-toggle="tab" href="#coupons"><i class="ti-cut"></i> Coupons <span class="badge badge-danger">15</span></a> </li>
                             <li class=""><a data-toggle="tab" href="#deals"><i class="ti-link"></i>Deals <span class="badge badge-danger">15</span></a> </li> --}}
                          </ul>
                       </div>
                    </div>
                     <!-- end: Widget -->
                     <div class="tab-content">
                     <div role="tabpanel" class="tab-pane single-coupon active" id="popular">
                     <div class="row">
                       @foreach ($list as $stores)
                         <div class="col-sm-3 thumb">
                            <div class="widget">
                              <a href="/stores/{{$stores->name}}">
                               <div class="thumb-inside">
                                  <div class="thumbnail" title="">
                                  <img src="{{config('APP.URL')}}/shoplogo/{{$stores->store_logo}}" class="img-responsive" alt="">
                                </div>
                                  <div class="store_name text-center" content="">{{substr($stores->name, 0, 25)}}</div>

                                  {{-- <h4 class="coupon-title">{{$stores->address}}</h4> --}}

                               </div>
                               </a>
                               <!--//coupon block -->
                            </div>
                         </div>
                       @endforeach

                     </div>
                     </div>
                     <div role="tabpanel" class="tab-pane single-coupon " id="inStore">
                     <div class="row">
                       @if (Auth::user())
                         @php

                         $uid = Auth::user()->id;
                         $favourites = Favouritestore::where('user_id', $uid)->get();
                       @endphp

                       @foreach ($favourites as $favrits)
                         @php
                         $stores_list = Shop::where('id', $favrits->store_id)->get();
                         @endphp
                         @foreach ($stores_list as $stores1)

                           <div class="col-sm-3 thumb">
                             <div class="widget">
                               <a href="/stores/{{$stores1->name}}">
                                 <div class="thumb-inside">
                                   <div class="thumbnail" title="">
                                     <img src="{{config('APP.URL')}}/shoplogo/{{$stores1->store_logo}}" class="img-responsive" alt="">
                                   </div>
                                   <div class="store_name text-center" content="">{{$stores1->name}}</div>

                                   {{-- <h4 class="coupon-title">{{$stores->address}}</h4> --}}

                                 </div>
                               </a>
                               <!--//coupon block -->
                             </div>
                           </div>
                         @endforeach
                       @endforeach
                       @endif

                     </div>
                     </div>
                     </div>

                     <ul class="pagination pagination-lg m-t-0" style="">
                        {{-- <li>
                           <a href="#"> <i class="ti-arrow-left"></i>
                           </a>
                        </li>
                        <li> <a href="#">1</a>
                        </li>
                        <li class="active"> <a href="#">2</a>
                        </li>
                        <li> <a href="#">3</a>
                        </li>
                        <li> <a href="#">4</a>
                        </li>
                        <li>
                           <a href="#"> <i class="ti-arrow-right"></i>
                           </a>
                        </li> --}}
                     </ul>
                  </div>
               </div>
            </div>
         </section>
         <!-- Footer -->
           @include('backend.includes.mainfooter')
         <!-- start modal -->
       <!-- Large modal -->
         <div class="coupon_modal modal fade couponModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
               <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ti-close"></i></span> </button>
                  <div class="coupon_modal_content">

                     <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 text-center">
                           <h2>Save 30% off New Domains Names</h2>
                           <p>Not applicable to ICANN fees, taxes, transfers,or gift cards. Cannot be used in conjunction with any other offer, sale, discount or promotion. After the initial purchase term.</p>
                        </div>

                     <div class="row">
                         <div class="col-sm-12">
                             <h5 class="text-center text-uppercase m-t-20 text-muted">Click below to get your coupon code</h5>
                         </div>

                        <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3"> <a href="#" target="_blank" class="coupon_code alert alert-info"><span class="coupon_icon"><i class="ti-cut hidden-xs"></i></span>  DAZ50-8715
                           </a>
                        </div>
                     </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="report">Did this coupon work? <span class="yes vote-link" data-src="#">Yes</span> <span class="no vote-link" data-src="#">No</span> </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- end: Coupon modal content -->
               </div>



                    <div class="newsletter-modal">
                     <div class="newsletter-form">
                        <h4><i class="ti-email"></i>Sign up for our weekly email newsletter with the best money-saving coupons.</h4>
                        <div class="input-group">
                           <input class="form-control input-lg" placeholder="Email" type="text"> <span class="input-group-btn">
                           <button class="btn btn-danger btn-lg" type="button">
                           Subscribe
                           </button>
                           </span>
                        </div>
                        <p><small>We’ll never share your email address with a third-party.</small> </p>
                        </div>
                    </div>
                     <ul class="nav nav-pills nav-justified">
                  <li role="presentation" class="active"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It worked"><i class="ti-check color-green"></i></a> </li>
                  <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="I love it"><i class="ti-heart color-primary"></i></a> </li>
                  <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-close"></i></a> </li>
               </ul>


            </div>
         </div>
         <!-- end: Modall -->
      </div>
      <!-- //wrapper -->
      <!-- jQuery  -->
      <script src="{{config('APP.URL')}}/assets/js/jquery.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/bootstrap.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/animsition.min.js"></script>
      <script src="{{config('APP.URL')}}/owl.carousel/owl.carousel.min.js"></script>
      <!-- Kupon js -->
      <script src="{{config('APP.URL')}}/assets/js/kupon.js"></script>
   </body>

<!-- Mirrored from codenpixel.com/demo/kuponhub/results_2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:54:38 GMT -->
</html>

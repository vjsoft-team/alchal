@php
  use App\Shop;
  use App\Store;
  use App\Category;
  use App\Coupon;
  $pageId = 3;
@endphp
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codenpixel.com/demo/kuponhub/store_profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:54:41 GMT -->
<head>
      <meta charset="utf-8" />
      <title>Coupons Based on Store</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta content="" name="description" />
      <meta content="Kupons" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <link rel="shortcut icon" href="#">
      <link href="{{config('APP.URL')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animate.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animsition.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css">
      <!-- Theme styles -->
      <link href="{{config('APP.URL')}}/assets/css/style.css" rel="stylesheet" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!-- Navigation Bar-->
         @include('backend.includes.header')
         <!-- Navigation ends -->
         <section class="results">
            <div class="dp-header">
               <div class="container">
                  <div class="row">
                     <div class="col-md-8 dph-info">
                        <img src="{{config('APP.URL')}}/shoplogo/{{$shop_list->first()->store_logo}}" class="profile-img" alt="">
                        <div>
                           <h4>{{$shop_list->first()->name}}</h4>
                           <p>{{$shop_list->first()->address}}
                           </p>
                           <p>Phone No - {{$shop_list->first()->contact_person_no}}</p>
                           <p>E@Mail: {{$shop_list->first()->email}}</p>
                           {{-- <a href="#">Electronics</a> <a href="#">Fashion</a> --}}
                        </div>
                     </div>
                     <div class="col-md-4 dph-reviews">
                        {{-- <p><span>9,2 &nbsp;<em>/10</em></span> 21 reviews</p> --}}
                        <p class="dph-rec"><i class="ti-cut"></i><span>{{count($list)}}</span> Offers</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container">
               <div class="row">
                  <div class="col-sm-3">
                     <div class="widget categories b-b-0">
                        <!-- /widget heading -->
                        <div class="widget-heading">
                           <h3 class="widget-title text-dark">
                              Related Categories
                           </h3>
                           <div class="clearfix"></div>
                        </div>
                        <div class="widget-body">
                           <!-- Sidebar navigation -->
                           <ul class="nav sidebar-nav">
                             @php
                               $categories = Category::take(5)->get();
                             @endphp
                             @foreach ($categories as $category_list)
                               <li>
                                  <a href="/category/{{$category_list->category}}"> <i class="ti-star">
                                  </i> {{$category_list->category}} <span class="sidebar-badge">
                                    @php
                                      $coupons_lists = Coupon::where('category', $category_list->category)->get();
                                    @endphp

                                  {{count($coupons_lists)}}
                                  </span> </a>
                               </li>
                             @endforeach

                           </ul>
                           <!-- Sidebar divider -->
                        </div>
                     </div>

                  </div>
                  <!--/col -->
                  <div class="col-sm-9">
                     <div class="widget-body">
                        <div class="widget">
                           <ul class="nav nav-tabs solo-nav responsive-tabs" id="myTab">
                              {{-- <li class="active"><a data-toggle="tab" href="#popular"><i class="ti-bar-chart"></i>Popular <span class="badge badge-info">78</span> </a> </li>
                              <li class=""><a data-toggle="tab" href="#coupons"><i class="ti-cut"></i> Coupons <span class="badge badge-danger">15</span></a> </li>
                              <li class=""><a data-toggle="tab" href="#deals"><i class="ti-link"></i>Deals <span class="badge badge-danger">15</span></a> </li> --}}
                              <li class=""><a data-toggle="tab" href="#inStore"><i class="ti-receipt"></i>In-store <span class="badge badge-purple">{{count($list)}}</span></a> </li>
                           </ul>
                        </div>
                     </div>
                     <!--/widget -->
                     <!-- Tab panes -->
                     <div class="tab-content">
                        <div role="tabpanel" class="tab-pane single-coupon active" id="popular">

                                 @foreach ($list as $coupon)
                                   <div class="coupon-wrapper coupon-single">
                                      <div class="row">
                                         <div class="ribbon-wrapper hidden-xs">
                                            {{-- <div class="ribbon"></div> --}}
                                         </div>
                                         <div class="coupon-data col-sm-2 text-center">
                                            <div class="savings text-center">
                                               <div>
                                                  <div class="large">{{$coupon->discount}}%</div>
                                                  <div class="small">off</div>
                                                  <div class="type">Coupon</div>
                                               </div>
                                            </div>
                                            <!-- end:Savings -->
                                         </div>
                                         <!-- end:Coupon data -->
                                   <div class="coupon-contain col-sm-7">
                                      <ul class="list-inline list-unstyled">
                                         {{-- <li class="sale label label-pink">Sale</li>
                                         <li class="popular label label-success">98% success</li>
                                         <li><span class="verified  text-success"><i class="ti-face-smile"></i>Verified</span> </li> --}}
                                         <li><span class="used-count">{{$coupon->used}} used</span> </li>
                                      </ul>
                                      @php
                                        $find_store = Store::find($coupon->store);
                                      @endphp
                                      <h4 class="coupon-title"><a href="#">{{$find_store->name}}|| {{$coupon->category}} || {{$coupon->title}}</a></h4>
                                      <p data-toggle="collapse" data-target="#1">{{$coupon->description}}</p>
                                      <p id="1" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                      <ul class="coupon-details list-inline">
                                         {{-- <li class="list-inline-item">
                                            <div class="btn-group" role="group" aria-label="...">
                                               <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                               <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                            </div>
                                            <!-- end:Btn group -->
                                         </li>
                                         <li class="list-inline-item">30% of 54 recommend</li> --}}
                                         <li class="list-inline-item"><a href="#">Share</a> </li>
                                      </ul>
                                      <!-- end:Coupon details -->
                                   </div>
                                   <div class="button-contain col-sm-3 text-center">
                                     @php
                                       $valitTill = Carbon\Carbon::createFromFormat('Y-m-d', $coupon->valid_to)->format('d-m-Y');
                                     @endphp
                                      <p class="btn-code" data-toggle="modal"> <span class="partial-code">Valid Till - {{$valitTill}}</span>  </p>
                                      {{-- <div class="btn-group" role="group" aria-label="...">
                                         <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                         <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                         <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                      </div> --}}
                                   </div>
                                 </div>
                                 <!-- //row -->
                              </div>
                                 @endforeach

                        </div>

                     </div>
                     <!-- end: Tab content -->
                     <!-- Poplura stores -->

                     <!-- end:Popular stores widget -->
                     <ul class="pagination pagination-lg m-t-0">
                        {{-- <li>
                           <a href="#"> <i class="ti-arrow-left"></i> </a>
                        </li>
                        <li> <a href="#">1</a> </li>
                        <li class="active"> <a href="#">2</a> </li>
                        <li> <a href="#">3</a> </li>
                        <li> <a href="#">4</a> </li>
                        <li>
                           <a href="#"> <i class="ti-arrow-right"></i> </a>
                        </li> --}}
                     </ul>
                  </div>
               </div>
               <div class="row">
                 <div class="widget">
                    <!-- /widget heading -->
                    <div class="widget-heading">
                       <h3 class="widget-title text-dark">
                          Top Stores
                       </h3>
                       <div class="widget-widgets"> <a href="#">View More Stores <span class="ti-angle-right"></span></a> </div>
                       <div class="clearfix"></div>
                    </div>
                    <div class="widget-body">
                       <div class="row">
                         @php
                           $logos = Store::orderBy('id','desc')->take(6)->get();
                         @endphp
                         @foreach ($logos as $value11)
                           <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 thumb">
                              <div class="thumb-inside">
                                 <a class="thumbnail" href="/stores/{{$value11->name}}"> <img class="img-responsive" src="{{config('APP.URL')}}/shoplogo/{{$value11->store_logo}}" alt=""> </a>
                                 <span class="favorite">
                                   <a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store">
                                   {{-- <i class="ti-heart"></i> --}}
                                 </a>
                               </span>
                              </div>
                              <div class="store_name text-center">
                                 <h5>{{$value11->name}}</h5>
                              </div>
                           </div>
                         @endforeach


                       </div>
                    </div>
                 </div>
               </div>
            </div>
         </section>
         <!-- Footer -->
         @include('backend.includes.mainfooter')
         <!-- start modal -->
        <!-- Large modal -->
        @foreach ($list as $coupon)
          <div class="coupon_modal modal fade couponModal{{$coupon->id}}" tabindex="-1" role="dialog" style="z-index: 99999">
             <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ti-close"></i></span> </button>
                   <div class="coupon_modal_content">

                      <div class="row">
                         <div class="col-sm-10 col-sm-offset-1 text-center">
                            <h2>{{$coupon->title}}</h2>
                            <p>Not applicable to taxes, transfers,or gift cards. Cannot be used in conjunction with any other offer, sale, discount or promotion. After the initial purchase term.</p>
                         </div>

                      <div class="row">
                          <div class="col-sm-12">
                              <h5 class="text-center text-uppercase m-t-20 text-muted">Click below to get your coupon code</h5>
                          </div>

                          @if (Auth::user())
                            <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                              <a href="#" target="_blank" class="coupon_code alert alert-info">
                                <span class="coupon_icon"><i class="ti-cut hidden-xs"></i></span>
                                {{$coupon->coupon}}
                              </a>
                            </div>
                            <div class="col-sm-12">
                               <div class="newsletter-form">
                                  <h4><i class="ti-mobile"></i>Send Coupoun code to mobile.</h4>
                                  <div class="input-group">
                                     <input type="hidden" name="coupon" value="{{$coupon->coupon}}">

                                     <input type="text" class="form-control input-lg" placeholder="Mobile Number" id="couponId{{$coupon->id}}"> <span class="input-group-btn">
                                     <button class="btn btn-danger btn-lg" onclick="sendMessage({{$coupon->id}}, '{{$coupon->coupon}}', {{Auth::user()->id}})" type="button">
                                       Send
                                     </button>
                                     </span>
                                  </div>
                                  <p><small>We’ll never share your Mobile Number with a third-party.</small> </p>
                               </div>
                            </div>
                            @else
                              <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                                <a href="/login">Please <strong> <u> Login </u></strong>to get Code..</a>
                              </div>
                          @endif
                      </div>
                         <div class="row">
                            <div class="col-sm-12">
                               {{-- <div class="report">Did this coupon work? <span class="yes vote-link" data-src="#">Yes</span> <span class="no vote-link" data-src="#">No</span> </div> --}}
                            </div>
                         </div>
                      </div>
                   </div>
                   <!-- end: Coupon modal content -->
                </div>



                     {{-- <div class="newsletter-modal">
                      <div class="newsletter-form">
                         <h4><i class="ti-email"></i>Sign up for our weekly email newsletter with the best money-saving coupons.</h4>
                         <div class="input-group">
                            <input class="form-control input-lg" placeholder="Email" type="text"> <span class="input-group-btn">
                            <button class="btn btn-danger btn-lg" type="button">
                            Subscribe
                            </button>
                            </span>
                         </div>
                         <p><small>We’ll never share your email address with a third-party.</small> </p>
                         </div>
                     </div> --}}
                      <ul class="nav nav-pills nav-justified">
                   <li role="presentation" class="active"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It worked"><i class="ti-check color-green"></i></a> </li>
                   <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="I love it"><i class="ti-heart color-primary"></i></a> </li>
                   <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-close"></i></a> </li>
                </ul>


             </div>
          </div>
        @endforeach

         <!-- end: Modall -->
      </div>
      <!-- //wrapper -->
      <!-- jQuery  -->
      <script src="{{config('APP.URL')}}/assets/js/jquery.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/bootstrap.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/animsition.min.js"></script>
      <script src="{{config('APP.URL')}}/owl.carousel/owl.carousel.min.js"></script>
      <!-- Kupon js -->
      <script src="{{config('APP.URL')}}/assets/js/kupon.js"></script>
      <script type="text/javascript">
      function sendMessage(couponId, couponCode, userId) {
        // console.log(storeId);
        // console.log(userId);
        var userNumber = $('#couponId' + couponId).val()
        console.log(userNumber);
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        data = {_token: CSRF_TOKEN, userNumber: userNumber, couponId: couponId, couponCode:couponCode, userId: userId};
        $.ajax({
          url: '/my-coupoun',
          type: 'POST',
          data: data,
          dataType: 'JSON',
          success: function (data) {
            console.log(data);
            // if (data.status == 'ok') {
            //   console.log('success');
            //   // $('#storeId' + storeId).attr('style', 'color: red');
            // }
          }
        });
      }
      </script>
   </body>

<!-- Mirrored from codenpixel.com/demo/kuponhub/store_profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:54:42 GMT -->
</html>

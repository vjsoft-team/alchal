@php
  use App\Coupon;
  use App\Shop;
  use App\Store;
  use App\Ads;
  use App\Category;
  use App\Favouritestore;

  $pageId = 1;
@endphp
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codenpixel.com/demo/kuponhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:53:44 GMT -->
<head>
      <meta charset="utf-8" />
      <title>Alchal Offers</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta content="" name="description" />
      <meta content="Kupons" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="csrf-token" content="{{ csrf_token() }}" />
      <link rel="shortcut icon" href="#">
      <link href="{{config('APP.URL')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animate.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animsition.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css">
      <!-- Theme styles -->
      <link href="{{config('APP.URL')}}/assets/css/style.css" rel="stylesheet" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!-- Navigation Bar-->
         @include('backend.includes.header')
         <!-- Navigation ends -->
         <div class="wrapper">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                     <!-- Main component for a primary marketing message or call to action -->
                     <div class="slide-wrap shadow">
                       @php
                         $adss = Ads::orderBy('position', 'asc')->get();
                       @endphp
                        <div class="main-slider">
                          @foreach ($adss as $key22)
                            <a href="#" class="item" data-hash="{{$key22->position}}"> <img src="{{config('APP.URL')}}/sliders/{{$key22->cover_image}}" alt=""> </a>
                          @endforeach
                           {{-- <div class="item" data-hash="two"> <img src="{{config('APP.URL')}}/assets/images/slide-01.jpg" alt=""> </div>
                           <div class="item" data-hash="three"> <img src="{{config('APP.URL')}}/assets/images/slide-03.jpg" alt=""> </div> --}}
                        </div>
                        <!-- /.carosuel -->
                        <div class="carousel-tabs clearfix">
                          @php
                            $adss = Ads::orderBy('position', 'asc')->get();
                            // var_dump($ads);
                          @endphp
                          @foreach ($adss as $ad)
                            <a class="col-sm-4 tab url" href="#{{$ad->position}}">
                              <div class="media">
                                <div class="media-left media-middle"> <img src="{{config('APP.URL')}}/sliders/{{$ad->logo}}" style="width: 80px;" alt=""> </div>
                                <div class="media-body" style="padding-top: 15px;">
                                  <h4 class="media-heading">{{substr($ad->title, 0, 25)}}</h4>
                                  <p>{{substr($ad->description, 0, 25)}}</p>
                                </div>
                              </div>
                            </a>
                          @endforeach
                        </div>
                     </div>
                     <!--/slide wrap -->
                  </div>
                  <!-- /col 12 -->
               </div>
               <div class="widget">
                  <!-- /widget heading -->
                  <div class="widget-heading">
                     <h3 class="widget-title text-dark">
                        Top Stores
                     </h3>
                     <div class="widget-widgets"> <a href="/stores">View More Stores <span class="ti-angle-right"></span></a> </div>
                     <div class="clearfix"></div>
                  </div>
                  <div class="widget-body">
                     <div class="row">
                       @php
                         $logos = Store::orderBy('id','desc')->take(6)->get();
                       @endphp
                       @foreach ($logos as $value11)
                         <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 thumb">
                            <div class="thumb-inside">
                               <a class="thumbnail" href="/stores/{{$value11->name}}"> <img class="img-responsive" src="{{config('APP.URL')}}/shoplogo/{{$value11->store_logo}}" alt=""> </a>
                               @if (Auth::user())
                                 @php
                                    $findStore = Favouritestore::where('user_id', Auth::user()->id)->where('store_id', $value11->id)->first();
                                 @endphp
                                 @if ($findStore['store_id'])
                                   <span class="favorite">
                                     <a class="save_store" onclick="saveStore({{$value11->id}}, {{Auth::user()->id}})" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store">
                                       <i class="ti-heart" style="color: red" id="storeId{{$value11->id}}"></i> </a>
                                     </span>
                                 @else
                                   <span class="favorite">
                                     <a class="save_store" onclick="saveStore({{$value11->id}}, {{Auth::user()->id}})" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store">
                                       <i class="ti-heart" id="storeId{{$value11->id}}"></i> </a>
                                     </span>
                                 @endif
                               @endif
                            </div>
                            <div class="store_name text-center">
                               <h5>{{$value11->name}}</h5>
                            </div>
                         </div>
                       @endforeach

                        <!-- /thumb -->
                        {{-- <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 thumb">
                           <div class="thumb-inside">
                              <a class="thumbnail" href="#"> <img class="img-responsive" src="{{config('APP.URL')}}/assets/images/thumb1.png" alt=""> </a> <span class="favorite"><a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store"><i class="ti-heart"></i></a></span>
                           </div>
                           <div class="store_name text-center">
                              <h5>Shopers</h5>
                           </div>
                        </div>
                        <!-- /thumb -->
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 thumb">
                           <div class="thumb-inside">
                              <a class="thumbnail" href="#"> <img class="img-responsive" src="{{config('APP.URL')}}/assets/images/thumb2.png" alt=""> </a> <span class="favorite"><a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store"><i class="ti-heart"></i></a></span>
                           </div>
                           <div class="store_name text-center">
                              <h5>Shoplogo</h5>
                           </div>
                        </div>
                        <!-- /thumb -->
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 thumb">
                           <div class="thumb-inside">
                              <a class="thumbnail" href="#"> <img class="img-responsive" src="{{config('APP.URL')}}/assets/images/thumb3.png" alt=""> </a> <span class="favorite"><a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store"><i class="ti-heart"></i></a></span>
                           </div>
                           <div class="store_name text-center">
                              <h5>Affiliate</h5>
                           </div>
                        </div>
                        <!-- /thumb -->
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 thumb">
                           <div class="thumb-inside">
                              <a class="thumbnail" href="#"> <img class="img-responsive" src="{{config('APP.URL')}}/assets/images/thumb4.png" alt=""> </a> <span class="favorite"><a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store"><i class="ti-heart"></i></a></span>
                           </div>
                           <div class="store_name text-center">
                              <h5>Coupons</h5>
                           </div>
                        </div>
                        <!-- /thumb -->
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 thumb">
                           <div class="thumb-inside">
                              <a class="thumbnail" href="#"> <img class="img-responsive" src="{{config('APP.URL')}}/assets/images/thumb5.png" alt=""> </a> <span class="favorite"><a href="#" data-toggle="tooltip" data-placement="left" title="" data-original-title="Save store"><i class="ti-heart"></i></a></span>
                           </div>
                           <div class="store_name text-center">
                              <h5>Discounts</h5>
                           </div>
                        </div> --}}
                        <!-- /thumb -->
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-8">
                     <ul class="nav nav-tabs responsive-tabs" id="myTab">
                        <li class="active"><a data-toggle="tab" href="#popular"><i class="ti-bar-chart"></i>Popular </a> </li>
                        {{-- <li class=""><a data-toggle="tab" href="#ending"><i class="ti-timer"></i> Ending soon</a> </li>
                        <li class=""><a data-toggle="tab" href="#online"><i class="ti-link"></i>Online</a> </li>
                        <li class=""><a data-toggle="tab" href="#atStore"><i class="ti-receipt"></i>In-store</a> </li> --}}
                     </ul>
                     <div class="tab-content clearfix" id="myTabContent">
                        <div id="popular" class="tab-pane counties-pane active animated fadeIn">
                          @php
                            $coupons_list = Coupon::orderBy('id', 'DESC')->take(4)->get();
                          @endphp
                          @foreach ($coupons_list as $coupon)


                          <div class="coupon-wrapper row">
                             <div class="coupon-data col-sm-2 text-center">
                                <div class="savings text-center">
                                   <div>
                                      <div class="large">{{$coupon->discount}}%</div>
                                      <div class="small">off</div>
                                      <div class="type">Coupon</div>
                                   </div>
                                </div>
                                <!-- end:Savings -->
                             </div>
                             <!-- end:Coupon data -->
                             <div class="coupon-contain col-sm-7">
                                <ul class="list-inline list-unstyled">
                                   {{-- <li class="sale label label-pink">Sale</li>
                                   <li class="label label-info">In store</li> --}}
                                   @php

                                   @endphp
                                   <li><span class="used-count">{{$coupon->used}} used</span> </li>
                                </ul>
                                @php
                                  $find_store = Store::find($coupon->store);
                                @endphp
                                <h4 class="coupon-title"><a href="#">{{$find_store->name}}|| {{$coupon->category}} || {{$coupon->title}}</a></h4>
                                <p data-toggle="collapse" data-target="#1">{{$coupon->description}}</p>
                                <p id="1" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                <ul class="coupon-details list-inline">
                                   {{-- <li class="list-inline-item">
                                      <div class="btn-group" role="group" aria-label="...">
                                         <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                         <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                      </div>
                                      <!-- end:Btn group -->
                                   </li>
                                   <li class="list-inline-item">30% of 54 recommend</li> --}}
                                   {{-- <li class="list-inline-item"><a href="#">Share</a> </li> --}}
                                </ul>
                                <!-- end:Coupon details -->
                             </div>
                             <!-- end:Coupon cont -->
                             <div class="button-contain col-sm-3 text-center">
                               @php
                                 $valitTill = Carbon\Carbon::createFromFormat('Y-m-d', $coupon->valid_to)->format('d-m-Y');
                               @endphp
                                <p class="btn-code" data-toggle="modal"> <span class="partial-code">Valid Till - {{$valitTill}}</span>  </p>
                                {{-- <div class="btn-group" role="group" aria-label="...">
                                   <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                   <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                   <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                </div> --}}
                             </div>
                          </div>

                          @endforeach
                        </div>

                        <div id="ending" class="tab-pane counties-pane animated fadeIn">
                           <div class="coupon-wrapper row">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">45%</div>
                                       <div class="small">off</div>
                                       <div class="type">Coupon</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="sale label label-pink">Sale</li>
                                    <li class="popular label label-success">100% success</li>
                                    <li><span class="verified  text-success"><i class="ti-face-smile"></i>Verified</span> </li>
                                    <li><span class="used-count">245 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">Clearance up to 50% off</a></h4>
                                 <p data-toggle="collapse" data-target="#more2">store these storename deals of the day to save as much...</p>
                                 <p id="more2" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                           <div class="coupon-wrapper row">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">50%</div>
                                       <div class="small">off</div>
                                       <div class="type">Coupon</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="promo label label-pink">Promo code</li>
                                    <li class="sale label label-warning">Ending</li>
                                    <li><span class="used-count">51 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">Jack Black Men's Pit Boss Antiperspirant & Deodorant</a></h4>
                                 <p data-toggle="collapse" data-target="#2">Buy online and pick-up in-store to save 15% or 20%...</p>
                                 <p id="2" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                           <div class="coupon-wrapper row">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">72%</div>
                                       <div class="small">off</div>
                                       <div class="type">Deal</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="sale label label-pink">Sale</li>
                                    <li><span class="used-count">27 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">Extra 10% Off Select Luggage + Up To $150 Back In Points For Members + Free Shipping</a></h4>
                                 <p data-toggle="collapse" data-target="#more3">store these storename deals of the day to save as much...</p>
                                 <p id="more3" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                           <div class="coupon-wrapper row">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">50%</div>
                                       <div class="small">off</div>
                                       <div class="type">Coupon</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="promo label label-pink">Promo code</li>
                                    <li class="sale label label-warning">Ending</li>
                                    <li><span class="used-count">51 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">Jack Black Men's Pit Boss Antiperspirant & Deodorant</a></h4>
                                 <p data-toggle="collapse" data-target="#more-2">Buy online and pick-up in-store to save 15% or 20%...</p>
                                 <p id="more-2" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                        </div>
                        <div id="online" class="tab-pane counties-pane animated fadeIn">
                           <div class="coupon-wrapper row featured">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">25%</div>
                                       <div class="small">off</div>
                                       <div class="type">Coupon</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="sale label label-pink">Sale</li>
                                    <li class="popular label label-success">100% success</li>
                                    <li><span class="verified  text-success"><i class="ti-face-smile"></i>Verified</span> </li>
                                    <li><span class="used-count">78 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">Up To 70% Off | storename Promo Codes &amp; Coupons</a></h4>
                                 <p data-toggle="collapse" data-target="3">store these storename deals of the day to save as much...</p>
                                 <p id="3" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                           <div class="coupon-wrapper row">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">15%</div>
                                       <div class="small">off</div>
                                       <div class="type">Coupon</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="promo label label-pink">Promo code</li>
                                    <li class="sale label label-warning">Ending</li>
                                    <li><span class="used-count">51 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">20% Off With In-Store Pick-Up</a></h4>
                                 <p data-toggle="collapse" data-target="4">Buy online and pick-up in-store to save 15% or 20%...</p>
                                 <p id="4" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                           <div class="coupon-wrapper row">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">72%</div>
                                       <div class="small">off</div>
                                       <div class="type">Deal</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="sale label label-pink">Sale</li>
                                    <li><span class="used-count">27 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">Extra 10% Off Select Luggage + Up To $150 Back In Points For Members + Free Shipping</a></h4>
                                 <p data-toggle="collapse" data-target="5">store these storename deals of the day to save as much...</p>
                                 <p id="5" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                           <div class="coupon-wrapper row">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">50%</div>
                                       <div class="small">off</div>
                                       <div class="type">Coupon</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="promo label label-pink">Promo code</li>
                                    <li class="sale label label-warning">Ending</li>
                                    <li><span class="used-count">51 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">Jack Black Men's Pit Boss Antiperspirant & Deodorant</a></h4>
                                 <p data-toggle="collapse" data-target="#more-3">Buy online and pick-up in-store to save 15% or 20%...</p>
                                 <p id="more-3" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                        </div>
                        <div id="atStore" class="tab-pane counties-pane animated fadeIn">
                           <div class="coupon-wrapper row featured">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">25%</div>
                                       <div class="small">off</div>
                                       <div class="type">Coupon</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="sale label label-pink">Sale</li>
                                    <li class="popular label label-success">100% success</li>
                                    <li><span class="verified  text-success"><i class="ti-face-smile"></i>Verified</span> </li>
                                    <li><span class="used-count">78 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">Up To 70% Off | storename Promo Codes &amp; Coupons</a></h4>
                                 <p data-toggle="collapse" data-target="6">store these storename deals of the day to save as much...</p>
                                 <p id="6" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                           <div class="coupon-wrapper row">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">15%</div>
                                       <div class="small">off</div>
                                       <div class="type">Coupon</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="promo label label-pink">Promo code</li>
                                    <li class="sale label label-warning">Ending</li>
                                    <li><span class="used-count">51 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">20% Off With In-Store Pick-Up</a></h4>
                                 <p data-toggle="collapse" data-target="#7">Buy online and pick-up in-store to save 15% or 20%...</p>
                                 <p id="7" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                           <div class="coupon-wrapper row">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">72%</div>
                                       <div class="small">off</div>
                                       <div class="type">Deal</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="sale label label-pink">Sale</li>
                                    <li><span class="used-count">27 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">Extra 10% Off Select Luggage + Up To $150 Back In Points For Members + Free Shipping</a></h4>
                                 <p data-toggle="collapse" data-target="#8">Shop these storename deals of the day to save as much...</p>
                                 <p id="8" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                           <div class="coupon-wrapper row">
                              <div class="coupon-data col-sm-2 text-center">
                                 <div class="savings text-center">
                                    <div>
                                       <div class="large">50%</div>
                                       <div class="small">off</div>
                                       <div class="type">Coupon</div>
                                    </div>
                                 </div>
                                 <!-- end:Savings -->
                              </div>
                              <!-- end:Coupon data -->
                              <div class="coupon-contain col-sm-7">
                                 <ul class="list-inline list-unstyled">
                                    <li class="promo label label-pink">Promo code</li>
                                    <li class="sale label label-warning">Ending</li>
                                    <li><span class="used-count">51 used</span> </li>
                                 </ul>
                                 <h4 class="coupon-title"><a href="#">Jack Black Men's Pit Boss Antiperspirant & Deodorant</a></h4>
                                 <p data-toggle="collapse" data-target="#more-4">Buy online and pick-up in-store to save 15% or 20%...</p>
                                 <p id="more-4" class="collapse">Don't miss out on all the coupon savings.Get you coupon now and save big</p>
                                 <ul class="coupon-details list-inline">
                                    <li class="list-inline-item">
                                       <div class="btn-group" role="group" aria-label="...">
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="It worked"><i class="ti-thumb-up"></i> </button>
                                          <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-thumb-down"></i> </button>
                                       </div>
                                       <!-- end:Btn group -->
                                    </li>
                                    <li class="list-inline-item">30% of 54 recommend</li>
                                    <li class="list-inline-item"><a href="#">Share</a> </li>
                                 </ul>
                                 <!-- end:Coupon details -->
                              </div>
                              <!-- end:Coupon cont -->
                              <div class="button-contain col-sm-3 text-center">
                                 <p class="btn-code" data-toggle="modal" data-target=".couponModal"> <span class="partial-code">BTSBAGS</span> <span class="btn-hover">Get Code</span> </p>
                                 <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-star"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-email"></i> </button>
                                    <button type="button" class="btn btn-default btn-xs"><i class="ti-mobile"></i> </button>
                                 </div>
                              </div>
                           </div>
                           <!-- end: coupon wrap -->
                        </div>
                     </div>
                     <!-- end: Tab content -->
                     <div class="clearfix"></div>

                     <ul class="nav nav-tabs" id="multitabs">
                        <li class="active"><a data-toggle="tab" href="#tab1">Popular stores</a> </li>
                        <li class=""><a data-toggle="tab" href="#tab2">Popular Categories</a> </li>
                     </ul>
                     <div class="tab-content clearfix" id="multitabsContent">
                        <div id="tab1" class="tab-pane counties-pane active">
                          @php
                            $shops = Store::all();
                          @endphp
                          @foreach ($shops as $shopvalue)
                            <div class="col-sm-3">
                               <div class="row coupons-cat">
                                  <div> <a href="/stores/{{$shopvalue->name}}">{{$shopvalue->name}}</a>
                                     <br>
                                  </div>
                               </div>
                            </div>
                          @endforeach


                        </div>
                        <div id="tab2" class="tab-pane">
                          @php
                            $categories = Category::all();
                          @endphp
                          @foreach ($categories as $categoryname)
                            <div class="col-sm-3">
                               <div class="row coupons-cat">
                                  <div> <a href="/category/{{$categoryname->category}}">{{$categoryname->category}}</a>
                                     <br>
                                  </div>
                               </div>
                            </div>
                          @endforeach

                        </div>
                     </div>
                     <!-- end: Tab content -->
                  </div>
                  <div class="col-lg-4">

                     <div class="widget">
                        <!-- /widget heading -->
                        <div class="widget-heading">
                           <h3 class="widget-title text-dark">
                              Search your Favourite Stores
                           </h3>
                           <div class="clearfix"></div>
                        </div>
                        <div class="widget-body">
                           <form class="form-horizontal select-search">
                              <label class="control-label ">What you searching for?</label>
                              <div class="btn-group" data-toggle="buttons">
                                 <label class="btn btn-default"> <i class="ti-tag"></i>
                                 <input type="checkbox" checked>Coupons</label>
                                 <label class="btn btn-default"> <i class="ti-cut"></i>
                                 <input type="checkbox">Discounts</label>
                                 <label class="btn btn-default active"> <i class="ti-alarm-clock"></i>
                                 <input type="checkbox">Deals</label>
                              </div>
                              <fieldset>
                                 <div class="form-group">
                                    <label class="control-label ">Keyword</label>
                                    <input class="form-control" id="text" name="text" type="text" />
                                 </div>
                                 <div class="row">
                                    <!-- Select Basic -->
                                    <div class="form-group col-sm-6 col-xs-12">
                                       <label class="control-label " for="category">Select category</label>
                                       <select class="select form-control" id="category" name="category">
                                         @php
                                           $catgories = Category::All();
                                         @endphp
                                         @foreach ($catgories as $categories)

                                           <option value="{{$categories->category}}">{{$categories->category}}</option>
                                         @endforeach

                                       </select>
                                    </div>
                                    <div class="form-group col-sm-6 col-xs-12">
                                       <label class="control-label " for="store">Select a store</label>
                                       <select class="select form-control" id="store" name="store">
                                         @php
                                           $shop_list = Store::all();
                                         @endphp
                                         @foreach ($shop_list as $value1)

                                           <option value="{{$value1->name}}">{{$value1->name}}</option>
                                         @endforeach

                                       </select>
                                    </div>
                                 </div>
                                 <!-- //row -->
                                 <!-- Button -->
                                 <div class="form-group ">
                                    <button id="search_btn" name="search_btn" class="btn btn-danger">Search coupons</button>
                                 </div>
                              </fieldset>
                           </form>
                        </div>
                     </div>
                     <!-- /widget -->
                     <div class="widget categories b-b-0">
                        <!-- /widget heading -->
                        <div class="widget-heading">
                           <h3 class="widget-title text-dark">
                              Popular categories
                           </h3>
                           <div class="clearfix"></div>
                        </div>
                        <div class="widget-body">
                           <!-- Sidebar navigation -->
                           <ul class="nav sidebar-nav">
                              @php
                                $categories = Category::All();
                              @endphp
                              @foreach ($categories as $category_list)
                                <li>
                                   <a href="/category/{{$category_list->category}}"> <i class="ti-star">
                                   </i> {{$category_list->category}} <span class="sidebar-badge">
                                     @php
                                       $coupons_lists = Coupon::where('category', $category_list->category)->get();
                                     @endphp

                                   {{count($coupons_lists)}}
                                   </span> </a>
                                </li>
                              @endforeach

                           </ul>
                           <!-- Sidebar divider -->
                        </div>
                     </div>
                     <!-- /widget -->
                     {{-- <div class="widget">

                        <div class="widget-heading">
                           <h3 class="widget-title text-dark">
                              Popular tags
                           </h3>
                           <div class="clearfix"></div>
                        </div>
                        <div class="widget-body">
                           <ul class="tags">
                              <li> <a href="#" class="tag">
                                 Coupons
                                 </a>
                              </li>
                              <li> <a href="#" class="tag">
                                 Discounts
                                 </a>
                              </li>
                              <li> <a href="#" class="tag">
                                 Deals
                                 </a>
                              </li>
                              <li> <a href="#" class="tag">
                                 Shopname
                                 </a>
                              </li>
                              <li> <a href="#" class="tag">
                                 Ebay
                                 </a>
                              </li>
                              <li> <a href="#" class="tag">
                                 Fashion
                                 </a>
                              </li>
                              <li> <a href="#" class="tag">
                                 Shoes
                                 </a>
                              </li>
                              <li> <a href="#" class="tag">
                                 Kids
                                 </a>
                              </li>
                              <li> <a href="#" class="tag">
                                 Travel
                                 </a>
                              </li>
                              <li> <a href="#" class="tag">
                                 Hosting
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div> --}}
                     <!-- /widget -->
                     {{-- <div class="widget trending-coupons">

                        <div class="widget-heading">
                           <h3 class="widget-title text-dark">
                              Trending Coupons
                           </h3>
                           <div class="clearfix"></div>
                        </div>
                        <div class="widget-body">
                           <div class="media">
                              <div class="media-left media-middle"> <img src="{{config('APP.URL')}}/assets/images/thmbsm-5.png" alt=""> </div>
                              <div class="media-body">
                                 <h4 class="media-heading">Upto 70% Rewards</h4>
                                 <p>Up to 70% off on Clothing ...</p>
                              </div>
                           </div>

                           <div class="media">
                              <div class="media-left media-middle"> <img src="{{config('APP.URL')}}/assets/images/thmbsm-6.png" alt=""> </div>
                              <div class="media-body">
                                 <h4 class="media-heading">Upto 70% Rewards</h4>
                                 <p>Up to 70% off on Clothing ...</p>
                              </div>
                           </div>

                           <div class="media">
                              <div class="media-left media-middle"> <img src="{{config('APP.URL')}}/assets/images/thmbsm-4.png" alt=""> </div>
                              <div class="media-body">
                                 <h4 class="media-heading">Up to 50% off Mens Summer Essentials at Clothing</h4>
                                 <p>Up to 70% off on Clothing ...</p>
                              </div>
                           </div>
                        </div>

                     </div> --}}
                     <!-- /widget -->
                  </div>
                  <!-- end col -->
               </div>
               <!-- End row -->
            </div>
            <section class="call-to-action">
               <div class="container">
                  <div class="row explain_group">
                     <div class="col-md-4">
                        <a class="item" rel="nofollow" href="#">
                           <div class="box-icon"> <i class="bg-danger ti-search"></i> </div>
                           <div class="box-info">
                              <h3>Search coupons</h3>
                              <h4>Find best money-saving coupons.</h4>
                              <div class="point"><i class="ti-check"></i><span>Find fresh coupons</span> </div>
                              <div class="point"><i class="ti-check"></i><span>Top Coupons & Offers</span> </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-md-4">
                        <a class="item" rel="nofollow" href="#">
                           <div class="box-icon"> <i class="bg-info ti-shopping-cart-full"></i> </div>
                           <div class="box-info">
                              <h3>Save your money</h3>
                              <h4>Find best money-saving coupons.</h4>
                              <div class="point"><i class="ti-check"></i><span>Find fresh coupons</span> </div>
                              <div class="point"><i class="ti-check"></i><span>Top Coupons & Offers</span> </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-md-4">
                        <a class="item" rel="nofollow" href="#">
                           <div class="box-icon"> <i class="bg-purple ti-gift"></i> </div>
                           <div class="box-info">
                              <h3>Earn your gifts</h3>
                              <h4>Find best money-saving coupons.</h4>
                              <div class="point"><i class="ti-check"></i><span>Find fresh coupons</span> </div>
                              <div class="point"><i class="ti-check"></i><span>Top Coupons & Offers</span> </div>
                           </div>
                        </a>
                     </div>
                  </div>
               </div>
            </section>
            <section class="newsletter-alert">
               {{-- <div class="container text-center">
                  <div class="col-sm-12">
                     <div class="newsletter-form">
                        <h4><i class="ti-email"></i>Sign up for our weekly email newsletter with the best money-saving coupons.</h4>
                        <div class="input-group">
                           <input type="text" class="form-control input-lg" placeholder="Email"> <span class="input-group-btn">
                           <button class="btn btn-danger btn-lg" type="button">
                           Subscribe
                           </button>
                           </span>
                        </div>
                        <p><small>We’ll never share your email address with a third-party.</small> </p>
                     </div>
                  </div>
               </div> --}}
            </section>
            <!-- end:Newsletter signup -->
            <!-- Footer -->
            @include('backend.includes.mainfooter')
            <!-- start modal -->
           <!-- Large modal -->

           @php
             $coupons_list = Coupon::all();
           @endphp
           @foreach ($coupons_list as $coupon)
             <div class="coupon_modal modal fade couponModal{{$coupon->id}}" tabindex="-1" role="dialog" style="z-index: 99999">
                <div class="modal-dialog modal-lg" role="document">
                   <div class="modal-content">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ti-close"></i></span> </button>
                      <div class="coupon_modal_content">

                         <div class="row">
                            <div class="col-sm-10 col-sm-offset-1 text-center">
                               <h2>{{$coupon->title}}</h2>
                               <p>Not applicable to taxes, transfers,or gift cards. Cannot be used in conjunction with any other offer, sale, discount or promotion. After the initial purchase term.</p>
                            </div>

                         <div class="row">
                             <div class="col-sm-12">
                                 <h5 class="text-center text-uppercase m-t-20 text-muted">Click below to get your coupon code (if Logged - In)</h5>
                             </div>

                                @if (Auth::user())
                                  <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                                    <span class="coupon_code alert alert-info">
                                      @php
                                        $find_store = Store::find($coupon->store);
                                      @endphp

                                      <span id="{{$coupon->coupon}}" onclick="sendMessage({{$coupon->id}}, '{{$coupon->coupon}}', '{{$find_store->name}}', {{Auth::user()->id}})" class="coupon_icon"><i class="ti-cut hidden-xs"></i>
                                      {{-- <button class="btn btn-danger btn-lg" onclick="sendMessage({{$coupon->id}}, '{{$coupon->coupon}}', {{Auth::user()->id}})" type="button"> --}}
                                      {{-- {{$coupon->coupon}} --}}
                                      Grab the deal
                                    </span>
                                  </div>
                                  {{-- <div class="col-sm-12">
                                     <div class="newsletter-form">
                                        <h4><i class="ti-mobile"></i>Send Coupoun code to mobile.</h4>
                                        <div class="input-group">
                                           <input type="hidden" name="coupon" value="{{$coupon->coupon}}">

                                           <input type="text" class="form-control input-lg" placeholder="Mobile Number" id="couponId{{$coupon->id}}"> <span class="input-group-btn">
                                             Send
                                           </button>
                                           </span>
                                        </div>
                                        <p><small>We’ll never share your Mobile Number with a third-party.</small> </p>
                                     </div>
                                  </div> --}}
                                  @else
                                    <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                                      <a href="/login">Please <strong> <u> Login </u></strong>to get Code..</a>
                                    </div>
                                @endif
                         </div>
                            <div class="row">
                               {{-- <div class="col-sm-12">
                                  <div class="report">Did this coupon work? <span class="yes vote-link" data-src="#">Yes</span> <span class="no vote-link" data-src="#">No</span> </div>
                               </div> --}}
                            </div>
                         </div>
                      </div>
                      <!-- end: Coupon modal content -->
                   </div>



                         <ul class="nav nav-pills nav-justified">
                      <li role="presentation" class="active"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It worked"><i class="ti-check color-green"></i></a> </li>
                      <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="I love it"><i class="ti-heart color-primary"></i></a> </li>
                      <li role="presentation"><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="It didn't work"><i class="ti-close"></i></a> </li>
                   </ul>


                </div>
             </div>
           @endforeach

         <!-- end: Modall -->
         </div>
      </div>
      <!-- //wrapper -->
      <!-- jQuery  -->
      <script src="{{config('APP.URL')}}/assets/js/jquery.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/bootstrap.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/animsition.min.js"></script>
      <script src="{{config('APP.URL')}}/owl.carousel/owl.carousel.min.js"></script>
      <!-- Kupon js -->
      <script src="{{config('APP.URL')}}/assets/js/kupon.js"></script>
      <script type="text/javascript">
        function saveStore(storeId, userId) {
          // console.log(storeId);
          // console.log(userId);
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

          data = {_token: CSRF_TOKEN, store_id: storeId, user_id:userId};
          $.ajax({
            url: '/favouritestores',
            type: 'POST',
            data: data,
            dataType: 'JSON',
            success: function (data) {
              if (data.status == 'ok') {
                $('#storeId' + storeId).attr('style', 'color: red');
              } else if (data.status == 'deleted') {
                $('#storeId' + storeId).attr('style', 'color: black');
              }
            }
          });
        }

        function sendMessage(couponId, couponCode, store, userId) {
          // console.log(store);
          // console.log(userId);
          var userNumber = $('#couponId' + couponId).val()
          // console.log(userNumber);
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

          data = {_token: CSRF_TOKEN, couponId: couponId, couponCode:couponCode, store:store, userId: userId};
          $.ajax({
            url: '/my-coupoun',
            type: 'POST',
            data: data,
            dataType: 'JSON',
            success: function (data) {
              console.log(data);
              var divId = '#' + couponCode;
              $(divId).text('YOu got the Deal');
              // if (data.status == 'ok') {
              //   console.log('success');
              //   // $('#storeId' + storeId).attr('style', 'color: red');
              // }
            }
          });
        }
      </script>
   </body>

</html>

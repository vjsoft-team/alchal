<div class="container-fluid container-fixed-lg footer">
<div class="copyright sm-text-center">
<p class="small no-margin pull-left sm-pull-reset">
<span class="font-montserrat">Copyright &copy; {{Date('Y')}}</span>.
<span class="hint-text">Alchal</span>
<span class="hint-text">All rights reserved. </span>
<span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
</p>
<p class="small no-margin pull-right sm-pull-reset">
<a href="#">Hand-crafted</a> <span class="hint-text">&amp; Made with Love ® by vJsOFT</span>
</p>
<div class="clearfix"></div>
</div>
</div>

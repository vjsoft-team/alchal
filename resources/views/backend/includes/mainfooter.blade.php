<footer id="footer">
   <div class="btmFooter">
      <div class="container">
         <div class="row">
            <div class="col-sm-10 col-sm-offset-1 text-center">
               <ul class="list-inline list-unstyled">
                  <li><a href="/terms-and-conditions">Terms & Conditions</a> </li>
                  <li><a href="/privacy-policy">Privacy Policy</a> </li>

               </ul>
            </div>
         </div>
         <div class="col-sm-12 text-center m-t-20">
            <p>
               Copyright {{Date('Y')}}
             <strong>Alchal</strong>. Crafted with<i class="ti-heart">
             </i>& Care  <strong>
               by <a href="http://vjsoft.org" target="_blank">vJsOFT</a>
               </strong>
            </p>
         </div>
         <div class="col-sm-12 text-center m-t-30">
            <ul class="pay-opt list-inline list-unstyled">
              <li>
                <a href="https://www.facebook.com/Allchall"> <i class="ti-facebook"> </i> </a>
              </li>
              <li>
                <a href="https://www.instagram.com/alchal.insta/"> <i class="ti-instagram"> </i> </a>
              </li>
              <li>
                <a href="https://twitter.com/AlchalTweet"> <i class="ti-twitter-alt"> </i> </a>
              </li>

            </ul>
         </div>
      </div>
   </div>
</footer>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114357819-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114357819-1');
</script>

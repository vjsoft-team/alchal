<header class="header">
   <div class="top-nav  navbar m-b-0 b-0">
      <div class="container">
         <div class="row">
            <!-- LOGO -->
            <div class="topbar-left col-sm-6 col-xs-4">
               <a href="/" class="logo"> <img src="{{config('APP.URL')}}/alchal.png" alt="" class="img-responsive"> </a>
            </div>
            <!-- End Logo container-->
            <div class="menu-extras col-sm-6 col-xs-8">
              <div class="menu-item">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle">
                  <div class="lines"> <span></span> <span></span> <span></span> </div>
                </a>
                <!-- End mobile menu toggle-->
              </div>
               <ul class="nav navbar-nav navbar-right pull-right">
                  <li>
                     <form role="search" class="app-search pull-left hidden-xs">
                        <div class="input-group">
                           <input class="form-control" placeholder="Search coupons ..." aria-label="Text input with multiple buttons">
                        </div>
                        <a href="#"><i class="ti-search"></i></a>
                     </form>
                  </li>

                  <li class="dropdown user-box">
                     <a href="#" class="dropdown-toggle profile btn btn-default" data-toggle="dropdown" aria-expanded="false">
                     My account
                     </a>
                     <ul class="dropdown-menu" style="margin-top:16px">
                       @if (Auth::user())
                         <li><a href="/profile/create"> My Account</a> </li>
                         <li><a href="/user/logout"> Logout</a> </li>
                         @else
                           <li><a href="/login"> LogIn</a> </li>
                           <li><a href="/register"> Register</a> </li>

                       @endif
                     </ul>
                  </li>

                  {{-- <li class="dropdown user-box">
                    <a href="#" class="dropdown-toggle profile btn btn-default" data-toggle="dropdown" aria-expanded="true">
                     My account
                     </a>
                     <ul class="dropdown-menu" style="margin-top:16px">
                        <li><a href="javascript:void(0)"> LogIn</a> </li>
                        <li><a href="javascript:void(0)">Registration</a> </li>
                        <li><a href="javascript:void(0)">Help Center </a> </li>
                     </ul>
                  </li> --}}
               </ul>

            </div>
         </div>
      </div>
   </div>
   <div class="navbar-custom shadow">
      <div class="container">
         <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
               <li class="@if ($pageId == 1)
                 active
               @endif"> <a href="/"><i class="ti-home"></i> <span> Home </span> </a> </li>
             <li class="@if ($pageId == 2)
               active
             @endif">
                  <a href="/coupons"><i class="ti-cut"></i> <span> Coupons </span> </a>
                  {{-- <ul class="submenu">
                     <li><a href="results.html">Coupon list</a> </li>
                     <li><a href="results_2.html">Coupon grid</a> </li>
                     <li><a href="results_3.html">Coupon grid image</a> </li>
                  </ul> --}}
               </li>
             <li class="@if ($pageId == 3)
               active
             @endif">
                  <a href="/stores"><i class="ti-shopping-cart"></i> <span> Stores </span> </a>

               </li>
                <li class="@if ($pageId == 4)
                  active
                @endif">
                  <a href="/contact-us"><i class="ti-announcement"></i> <span> Contact-Us </span> </a>

                </li>
             {{--   <li class="has-submenu">
                  <a href="#"><i class="ti-layout-list-thumb"></i> <span> Pages </span> </a>
                  <ul class="submenu">
                     <li><a href="store_profile.html">Store</a> </li>
                     <li><a href="categories.html">Search stores</a> </li>
                     <li><a href="results.html">Coupos list</a> </li>
                     <li><a href="results_2.html">Coupons grid</a> </li>
                     <li><a href="results_3.html">Coupons grid images</a> </li>
                     <li><a href="submit.html">Submit coupon</a> </li>
                     <li><a href="faq.html">Faq</a> </li>
                     <li><a href="pricing.html">Pricing</a> </li>
                     <li><a href="contact.html">Contact</a> </li>
                     <li><a href="features.html">Features</a> </li>
                     <li><a href="blog_single.html">Blog</a> </li>
                     <li><a href="blog_articles.html">Blog list</a> </li>
                  </ul>
               </li> --}}
            </ul>
            <!-- End navigation menu  -->
         </div>
      </div>
   </div>
</header>

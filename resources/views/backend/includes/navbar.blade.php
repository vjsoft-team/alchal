
<div class="header">

<div class="container-fluid relative">

<div class="pull-left full-height visible-sm visible-xs">

<div class="header-inner">
<a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
<span class="icon-set menu-hambuger"></span>
</a>
</div>

</div>
<div class="pull-center hidden-md hidden-lg">
<div class="header-inner">
<div class="brand inline">
<img src="{{config('APP_URL')}}/alchal_admin_logo.png" alt="logo" data-src="{{config('APP_URL')}}/backend/assets/img/logo.png" data-src-retina="{{config('APP_URL')}}/alchal_admin_logo.png" width="78" height="22">
</div>
</div>
</div>


</div>

<div class="pull-left sm-table hidden-xs hidden-sm">
<div class="header-inner">
<div class="brand inline">
<img src="{{config('APP_URL')}}/alchal_admin_logo.png" alt="logo" data-src="{{config('APP_URL')}}/alchal_admin_logo.png" data-src-retina="{{config('APP_URL')}}/alchal_admin_logo.png" width="78" height="22">
</div>

<ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20">

</ul>

</div>
</div>

<div class="pull-right">

<div class="visible-lg visible-md m-t-10">
<div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
  @if (Auth::user())

    <span class="semi-bold">{{Auth::user()->name}}</span>
  @endif
</div>
<div class="dropdown pull-right">
<button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<span class="thumbnail-wrapper d32 circular inline m-t-5">
<img src="{{config('APP.URL')}}/a1.jpg" alt="" data-src="{{config('APP.URL')}}/a1.jpg" data-src-retina="{{config('APP.URL')}}/a1.jpg" width="32" height="32">
</span>
</button>
<ul class="dropdown-menu profile-dropdown" role="menu">
<li><a href="#"><i class="pg-settings_small"></i> Settings</a>
</li>
<li><a href="#"><i class="pg-outdent"></i> Feedback</a>
</li>
<li><a href="#"><i class="pg-signals"></i> Help</a>
</li>
<li class="bg-master-lighter">
@if (Auth::guard('admin')->check())
    <a href="{{ route('admin.logout') }}" class="clearfix"   onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
               <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                   {{ csrf_field() }}
               </form>
    <span class="pull-left">Admin Logout</span>
    <span class="pull-right"><i class="pg-power"></i></span>
    </a>
@elseif (Auth::guard('web')->check())
  <a href="{{ route('user.logout') }}" class="clearfix"   onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
             <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                 {{ csrf_field() }}
             </form>
  <span class="pull-left">Logout</span>
  <span class="pull-right"><i class="pg-power"></i></span>
  </a>
@elseif (Auth::guard('store')->check())
  <a href="{{ route('validation.logout') }}" class="clearfix"   onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
             <form id="logout-form" action="{{ route('validation.logout') }}" method="POST" style="display: none;">
                 {{ csrf_field() }}
             </form>
  <span class="pull-left">Store Logout</span>
  <span class="pull-right"><i class="pg-power"></i></span>
  </a>
@endif

</li>
</ul>
</div>
</div>

</div>
</div>

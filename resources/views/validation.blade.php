@php
  use App\User;
@endphp
@extends('layouts.sidebarapp')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Validate Coupoun</div>

                <div class="panel-body">
                  <form class="" action="{{route('validation.validate')}}" method="post">
                    <div class="col-md-6">
                      <div class="form-group">
                        {{ csrf_field() }}
                        <input type="text" class="form-control" name="validate" value="" >
                        <input type="hidden" class="form-control" name="store" value="Zero degrees Cave" >
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <input type="submit" class="btn btn-warning" name="submit" value="Validate">
                      </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
  @isset($valid)
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">
            @php
            $findUser = User::find($valid->first()->user_id);
            // var_dump($findUser);

            @endphp
            {{$findUser->name}} Coupoun Valid
          </div>
        </div>
      </div>
    </div>
  @endisset
</div>
@endsection


  <!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codenpixel.com/demo/kuponhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:53:44 GMT -->
<head>
      <meta charset="utf-8" />
      <title>{{config('app.name')}}</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta content="" name="description" />
      <meta content="Kupons" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="#">
      <link href="{{config('APP.URL')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animate.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animsition.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css">
      <!-- Theme styles -->
      <link href="{{config('APP.URL')}}/assets/css/style.css" rel="stylesheet" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!-- Navigation Bar-->
         @include('backend.includes.header')
         <!-- Navigation ends -->
         <div class="wrapper">
           <section class="contact m-t-30">
              <div class="container">
                 <div class="row">
                    <!-- REGISTER -->
                    <div class="col-md-12">
                       <div class="widget">

                          <div class="widget-body">
                            <h3>Terms & Conditions</h3>
                            <h4>ALCHAL Terms of Use</h4>
                            <br>
                            <p>
                              The domain name www.Alchal.com (hereinafter referred to as "Website") is owned by
                              Hello tirupati A to y services a company incorporated under the partnership Act, 1932
                              with its registered office at 13-30-S11-707, Pk Layout, Tirupati - 517501​..</p><br>
                            <p>For the purpose of these Terms of Use, wherever the context so requires "You" or
                              "User" shall mean any natural or legal person who has agreed to become a user on the
                              Website by providing Registration Data while registering on the Website as Registered
                              User using the computer systems. The term "We", "Us", "Our" shall mean Alchal.com.</p>
                              <p>Alchal provides its services to you subject to the notices, terms, and conditions set forth
                                in this agreement (the "Agreement"). In addition, when you use any Alchal.com service
                                (e.g., Customer Reviews), you will be subject to the rules, guidelines, policies, terms,
                                and conditions applicable to such service, and they are incorporated into this
                                Agreement by this reference. By mere use of the Website, You shall be contracting with
                                Alchal and these terms and conditions including the policies constitute your binding
                                obligations, with Alchal.</p>
                                <p>Alchal reserves the right to change this Site and these terms and conditions at any time.
                                  Alchal retains the right to deny access to anyone who we believe has violated any of
                                  these Terms of Use. If you are under 18, you may use the Website only with
                                  involvement of a parent or guardian. ACCESSING, BROWSING OR OTHERWISE
                                  USING THE SITE INDICATES YOUR AGREEMENT TO ALL THE TERMS AND
                                  CONDITIONS IN THIS AGREEMENT, SO PLEASE READ THIS AGREEMENT
                                  CAREFULLY BEFORE PROCEEDING.</p>
                                <h4>Platform for Transaction and Communication</h4>  <br>
                                <p>The Website is a platform that Users utilize to meet and interact with alchal listed stores
                                    for their transactions. ALCHAL is not and cannot be a party to or control in any manner
                                    any transaction between the Website's Users.
                                      </p>
                                      <p>Henceforward:</p>
                                      <p>1. All commercial/contractual terms are offered by and agreed to between users and
stores alone. The commercial/contractual terms include without limitation price, shipping
costs, payment methods, payment terms, date, period and mode of delivery, warranties
related to products and services and after sales services related to products and
services. ALCHAL does not have any control or does not determine or advise or in any
way involve itself in the offering or acceptance of such commercial/contractual terms
between the users and Stores.</p> <br>
<p>2. ALCHAl does not make any representation or warranty as to specifics (such as
quality, value, salability, etc) of the copouns or services proposed to be sold or offered
to be sold or purchased on the Website. ALCHAL does not implicitly or explicitly support
or endorse the sale or purchase of any products or services on the Website. ALCHAL
accepts no liability for any errors or omissions, whether on behalf of itself or third
parties</p>           <br>
<p>3. ALCHAL is not responsible for any non-performance or breach of any contract
entered into between users and Stores. Alchal cannot and does not guarantee that the
concerned users and/or Stores will perform any transaction concluded on the Website.
Alchal shall not and is not required to mediate or resolve any dispute or disagreement
between User and Stores.
</p><br>
<p>4. Alchal does not make any representation or warranty as to the coupon -specifics
(such as legal title, creditworthiness, identity, etc) of any of its Users. You are advised to
independently verify the bona fides of any particular User that You choose to deal with
on the Website and use Your best judgment in that behalf.
</p><br>
<p>5. Alchal does not at any point of time during any transaction between user and stores
on the Website come into or take possession of any of the coupons or services offered
by Stores nor does it at any point gain title to or have any rights or claims over the
products or services offered by user to stores.</p><br>
<p>6. At no time shall Alchal hold any right, title or interest over the products nor shall
Alchal have any obligations or liabilities in respect of such contract entered into between
users and stores. Alchal is not responsible for unsatisfactory or delayed performance of
services or damages or delays as a result of products which are out of stock,
unavailable or back ordered.
</p><br>
<p>7. The Website is only a platform that can be utilized by Users to reach a larger base to
use and redeem coupons or services. Alchal is only providing a platform for
communication and it is agreed that the contract for sale of any of the products or
services shall be a strictly bipartite contract between the user and the Stores.</p><br>
<p>9. Alchal is not responsible for unsatisfactory or delayed performance of services or
damages or delays as a result of products which are out of stock, unavailable or back
ordered.
</p> <br>
<h4>Contents Posted on Site
</h4><br>
<p>Alchal shall be entitled to, consistent with our Privacy Policy, use the Content or any of
its elements for any type of use forever, including but not limited to promotional and
advertising purposes and in any media whether now known or hereafter devised,
including the creation of derivative works that may include Content you provide. You
agree that any Content you post may be used by Alchal, consistent with our Privacy
Policy and Rules of Conduct on Site as mentioned herein, and you are not entitled to
any payment or other compensation for such use.</p><br>
NEED TO EXPLAIN ABOUT PAYMENT TERMS <br>
EXPLAIN REFUND POLICY  <br>
EXPLAIN SHIPPING POLICY <br>
ESCALATION STAGES <br>
ANY LEGAL JUDICATION AT TIRUPATI ONLY <br>
<h4>Rules of Conduct On Site
</h4><br>
<p>1. You may not post, distribute, or reproduce in any way any copyrighted material,
trademarks, or other proprietary information without obtaining the prior written consent
of the owner of such proprietary rights.</p><br>
<p>2. ALCHAL may review, edit, reject, refuse to post and/or delete any Content that in the
sole judgment of Alchal violate these Terms of Use or which might be offensive, illegal,
or that might violate the rights, harm, or threaten the safety of another person.
</p><br>
<p>3. Any information provided by you on this site shall not be misleading in any way.</p><br>
<p>4. You may not post or transmit Content, (even if made in a joking, sarcastic or
unintended manner), if it in any way:</p><br>
<p>a. is patently offensive to the online community, such as sexually explicit content, or
content that promotes obscenity, pedophilia, racism, bigotry, hatred or physical harm of
any kind against any group or individual;
</p><br>
<p>b. harasses or advocates harassment of another person;</p><br>
<p>c. involves the transmission of “junk mail,” “chain letters,” or unsolicited mass mailing or
“spamming”;</p><br>
<p>d. promotes illegal activities or conduct that is abusive, threatening, obscene,
defamatory or libellous;</p> <br>
<p>e. infringes upon or violates any third party's rights [(including, but not limited to,
intellectual property rights, rights of privacy (including without limitation unauthorized
disclosure of a person's name, email address, physical address or phone number) or
rights of publicity];</p><br>
<p>f. promotes an illegal or unauthorized copy of another person's copyrighted work (see
"Copyright complaint" below for instructions on how to lodge a complaint about
uploaded copyrighted material), such as providing pirated computer programs or links to
them, providing information to circumvent manufacture-installed copy-protect devices,
or providing pirated music or links to pirated music files;</p><br>
<p>g.contains restricted or password-only access pages, or hidden pages or images (those
not linked to or from another accessible page);</p><br>
<p>h. provides material that exploits people in a sexual, violent or otherwise inappropriate
manner or solicits personal information from anyone;</p><br>
<p>i. provides instructional information about illegal activities such as making or buying
illegal weapons, violating someone’s privacy, or providing or creating computer viruses;</p><br>
<p>j. contains video, photographs, or images of another person age 18 or older without his
or her express written consent and permission or those of any minor (regardless of
whether you have consent from the minor or his or her legal guardian).</p><br>
<p>k. tries to gain unauthorized access or exceeds the scope of authorized access (as
defined herein and in other applicable Codes of Conduct or End User Access and
License Agreements) to the Sites or to profiles, blogs, communities, account
information, bulletins, friend request, or other areas of the Sites or solicits passwords or
personal identifying information for commercial or unlawful purposes from other users;</p><br>
<p>l. engages in commercial activities and/or sales without Alchal’s prior written consent
such as contests, sweepstakes, barter, advertising and pyramid schemes, or the buying
or selling of “virtual” items related to the Sites. Throughout this Terms of Use, Alchal’s
“prior written consent” means a communication coming from Alchal’s legal department,
specifically in response to your request, and specifically addressing the activity or
conduct for which you seek authorization;
</p><br>
<p>m. solicits gambling or engages in any gambling activity which Alchal, in its sole
discretion, believes is or could be construed as being illegal; n.contains viruses, time
bombs, rojan horses, cancelbots, worms or other harmful or deleterious components or
devices;</p><br>
<p>o.interferes with another user's use and enjoyment of the Sites or any other individual's
user and enjoyment of similar services; or</p><br>
<p>p. refers to any website or URL that, in the sole discretion of Alchal, contains material
that is inappropriate for the Sites, contains content that would be prohibited on the Sites,
or violates the letter or spirit of these Terms of Use.
</p><br>
<p>5. You must use the Sites in a manner consistent with any and all applicable laws and
regulations. Illegal and/or unauthorized uses of the Sites, including without limitation,
collecting usernames and/or email addresses of registered members by electronic or
other means for the purpose of sending unsolicited email and unauthorized framing of
or linking to the Sites, are not permitted. This includes the unauthorized interception of
the data stream coming from or going into the Sites, as well as attempting to gain
unauthorized access to the Sites or exceeding your authorized access.
</p><br>
<p>6. You may not engage in advertising to, or solicitation of, other users through the Sites
to buy or sell any products or services, including, but not limited to, products or services
related to the Sites. You may not transmit any chain letters or unsolicited commercial or
junk email to other users via the Sites. It is also a violation of these Terms of Use to use
any information obtained from the Sites in order to harass, abuse, or harm another
person, or in order to contact, advertise to, solicit, or sell to another person outside of
the Sites without their prior explicit consent. In order to protect our users from such
advertising or solicitation, Alchal reserves the right to restrict the number of messages
or emails which a user may send to other users in any 24-hour period to a number
which Alchal deems appropriate in its sole discretion.
</p><br>
<p>7. You understand that Alchal has the right at all times to disclose any information
(including the identity of the persons providing information or materials on the Sites) as
necessary to satisfy any law, regulation or valid governmental request. This may
include, without limitation, disclosure of the information in connection with investigation
of alleged illegal activity or solicitation of illegal activity or in response to a lawful court
order or subpoena. In addition, we can (and you hereby expressly authorize us to)
disclose any information about you to law enforcement or other government officials, as
we, in our sole discretion, believe necessary or appropriate in connection with the
investigation and/or resolution of possible crimes, especially those that may involve
personal injury.</p><br>
<p>8. Alchal reserves the right, but has no obligation, to monitor the materials posted in the
public areas of the Sites. Alchal shall have the right to remove or edit any Content that
in its sole discretion violates, or is alleged to violate, any applicable law or either the
spirit or letter of these Terms of Use. Notwithstanding this right of Alchal, YOU REMAIN
SOLELY RESPONSIBLE FOR THE CONTENT OF THE MATERIALS YOU POST IN
THE PUBLIC AREAS OF THE SITES AND IN YOUR PRIVATE MESSAGES. Please be
advised that Content posted in public areas of the Sites does not necessarily reflect the
views of Alchal. In no event shall Alchal assume or have any responsibility or liability for
any Content posted or for any claims, damages or losses resulting from use of Content
and/or appearance of Content on the Sites. You hereby represent and warrant that you
have all necessary rights in and to all Content you provide and all information it contains
and that such Content shall not infringe any proprietary or other rights of third parties or
contain any libelous, tortious, or otherwise unlawful information.</p><br>
<p>9. Your correspondence or business dealings with, or participation in promotions of,
advertisers found on or through the Sites, including payment and delivery of related
goods or services, and any other terms, conditions, warranties or representations
associated with such dealings, are solely between you and such advertiser. Alchal shall
not be responsible or liable for any loss or damage of any sort incurred as the result of
any such dealings or as the result of the presence of such advertisers on the Sites.
</p><br>
<p>10. It is possible that other users (including unauthorized users or “hackers”) may post
or transmit offensive or obscene materials on the Sites and that you may be
involuntarily exposed to such offensive and obscene materials. It also is possible for
others to obtain personal information about you due to your use of the Sites, and that
the recipient may use such information to harass or injure you. Alchal does not approve
of such unauthorized uses but by using the Sites you acknowledge and agree that
Alchal is not responsible for the use of any personal information that you publicly
disclose or share with others on the Sites. Please carefully select the type of information
that you publicly disclose or share with others on the Sites.</p><br>
<p>11. Alchal shall have all the rights to take necessary action and claim damages that may
occur due to your involvement/participation in any way on your own or through group/s
of People, intentionally or unintentionally in DoS/DDoS (Distributed Denial of Services).
</p><br>
<h4>Selling</h4> <br>
<p>As a registered seller, you are allowed to list item(s) for sale on the Website in
accordance with the Policies which are incorporated by way of reference in this Terms
of Use. You must be legally able to sell the item(s) you list for sale on our Website. You
must ensure that the listed items do not infringe upon the intellectual property, trade
secret or other proprietary rights or rights of publicity or privacy rights of third parties.
Listings may only include text descriptions, graphics and pictures that describe your
item for sale. All listed items must be listed in an appropriate category on the Website.
All listed items must be kept in stock for successful fulfilment of sales.</p><br>
<p>The listing description of the item must not be misleading and must describe actual
condition of the product. If the item description does not match the actual condition of
the item, you agree to refund any amounts that you may have received from the Buyer.
You agree not to list a single product in multiple quantities across various categories on
the Website. Alchal reserves the right to delete such multiple listings of the same
product listed by you in various categories.</p><br>
<h4>Other Businesses</h4><br>
<p>Alchal does not take responsibility or liability or warrant including but not limited to, the
actions, products, content and services on Alchal’s website, which are linked through
including but not limited to Affiliates and sites using Alchal’s APIs. In addition, Alchal
provide links to the sites of affiliated companies and certain other businesses for which,
Alchal is not responsible for examining or evaluating the products and services offered
by them, and we do not warrant the offerings of, any of these businesses or individuals
or the content of their website.</p><br>
<h4>Links</h4><br>
<p>This site may contain links to other web sites or you may be linked through other web
sites ("Linked Sites"). The Linked Sites are for your convenience only and you access
them at your own risk. We are not responsible for the content of the Linked Sites,
whether or not Alchal is affiliated with sponsors of the sites.</p><br>
<p>We do not in any way endorse the Linked Sites.
</p><br>
<p>We welcome links to this site. You may establish a hypertext link to this site, provided
that the link does not state or imply any sponsorship or endorsement of your site by
Alchal. You must not use on your site or in any other manner any trademarks, service
marks or any other materials appearing on the Website, including any logos or
characters, without the express written consent of the owner of the mark or materials.
You must not frame or otherwise incorporate into another web site or present in
conjunction with or juxtaposed against such a web site any of the content or other
materials on the Website without our prior written consent.</p><br>

                          </div>
                       </div>
                       <!-- end: Widget -->
                    </div>
                    <!-- /REGISTER -->
                    <!-- WHY? -->

                    <!-- /WHY? -->
                 </div>
              </div>
           </section>


            <!-- Footer -->
            @include('backend.includes.mainfooter')
            <!-- start modal -->
           <!-- Large modal -->


         <!-- end: Modall -->
         </div>
      </div>
      <!-- //wrapper -->
      <!-- jQuery  -->
      <script src="{{config('APP.URL')}}/assets/js/jquery.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/bootstrap.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/animsition.min.js"></script>
      <script src="{{config('APP.URL')}}/owl.carousel/owl.carousel.min.js"></script>
      <!-- Kupon js -->
      <script src="{{config('APP.URL')}}/assets/js/kupon.js"></script>
   </body>

<!-- Mirrored from codenpixel.com/demo/kuponhub/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:54:20 GMT -->
</html>

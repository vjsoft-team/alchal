@php
  use App\Favouritestore;
  use App\Shop;
@endphp
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from codenpixel.com/demo/kuponhub/results_2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:54:35 GMT -->
<head>
      <meta charset="utf-8" />
      <title>Profile</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta content="" name="description" />
      <meta content="Kupons" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="#">
      <link href="{{config('APP.URL')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animate.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/assets/css/animsition.min.css" rel="stylesheet" type="text/css">
      <link href="{{config('APP.URL')}}/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css">
      <!-- Theme styles -->
      <link href="{{config('APP.URL')}}/assets/css/style.css" rel="stylesheet" type="text/css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
         <!-- Navigation Bar-->
         @include('backend.includes.header')
         <!-- Navigation ends -->

         <section class="results m-t-30">
            <div class="container">
               <div class="row">
                  <div class="col-sm-3">


                  </div>
                  <!--/col -->
                  <div class="col-sm-6">
                    <div class="widget-body">
                       <div class="widget">
                          <ul class="nav nav-tabs solo-nav responsive-tabs" id="myTab">
                            <li class="active"><a data-toggle="tab" href="#general"><i class="ti-settings"></i>General Settings  </a> </li>
                            @php
                              $uid = Auth::user()->id;
                              $favourites = Favouritestore::where('user_id', $uid)->get();

                            @endphp


                              <li class=""><a data-toggle="tab" href="#password"><i class="ti-lock"></i>Password Updates </a> </li>

                                 {{--  <li class=""><a data-toggle="tab" href="#coupons"><i class="ti-cut"></i> Coupons <span class="badge badge-danger">15</span></a> </li>
                             <li class=""><a data-toggle="tab" href="#deals"><i class="ti-link"></i>Deals <span class="badge badge-danger">15</span></a> </li> --}}
                          </ul>
                       </div>
                    </div>
                     <!-- end: Widget -->
                     <div class="tab-content">
                     <div role="tabpanel" class="tab-pane single-coupon active" id="general">
                     <div class="widget">
                       <div class="widget-body">
                         <form class="form-horizontal select-search" method="POST" action="{{ action('ProfileController@store') }}">
                            {{ csrf_field() }}

                            <fieldset>
                              <input type="hidden" name="id" value="{{Auth::user()->id}}">
                              <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                 <label class="control-label ">Email</label>
                                 <input class="form-control" id="email" type="email"  name="email" value="{{ Auth::user()->email }}" disabled required  />
                                 @if ($errors->has('email'))
                                     <span class="help-block">
                                         <strong>{{ $errors->first('email') }}</strong>
                                     </span>
                                 @endif
                              </div>
                              <div class="row">

                              <div class="col-lg-6">
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                   <label class="control-label ">Name</label>
                                   <input class="form-control" id="name" type="text"  name="name" value="{{ Auth::user()->name }}" required autofocus />
                                   @if ($errors->has('name'))
                                       <span class="help-block">
                                           <strong>{{ $errors->first('name') }}</strong>
                                       </span>
                                   @endif
                                </div>
                              </div>
                              <div class="col-lg-6">
                                <div class="form-group {{ $errors->has('mobile') ? ' has-error' : '' }}">
                                   <label class="control-label ">Mobile</label>
                                   <input class="form-control" id="mobile" type="text"  name="mobile" maxlength="10" onkeypress='validate(event)' value="{{Auth::user()->mobile}}" required/>
                                   @if ($errors->has('mobile'))
                                       <span class="help-block">
                                           <strong>{{ $errors->first('mobile') }}</strong>
                                       </span>
                                   @endif
                                </div>
                              </div>
                            </div>




                               <!-- //row -->
                               <!-- Button -->
                               <div class="form-group ">
                                 <div class="col-md-3">

                                 </div>
                                 <div class="col-md-6">

                                   <button type="submit" name="submit" class="btn btn-danger">
                                     Update
                                   </button>

                                 </div>

                                  {{-- <button id="search_btn" name="search_btn" class="btn btn-danger">Search coupons</button> --}}
                               </div>
                            </fieldset>
                         </form>
                       </div>
                     </div>
                     </div>

                     <div role="tabpanel" class="tab-pane single-coupon " id="password">
                       <div class="widget">
                         <div class="widget-body">
                           <form class="form-horizontal select-search" method="POST" action="{{ url('/profile/update') }}">
                              {{ csrf_field() }}

                              <fieldset>
                                <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                   <label class="control-label ">Password</label>
                                   <input class="form-control" id="password" type="password"  name="password" required/>
                                   @if ($errors->has('password'))
                                       <span class="help-block">
                                           <strong>{{ $errors->first('password') }}</strong>
                                       </span>
                                   @endif
                                </div>
                                <div class="form-group">
                                   <label class="control-label ">Confirm Password</label>
                                   <input class="form-control" id="password-confirm" type="password" name="password_confirmation" required/>

                                </div>

                                 <!-- //row -->
                                 <!-- Button -->
                                 <div class="form-group ">
                                   <div class="col-md-6">

                                     <button type="submit" name="submit" class="btn btn-danger">
                                       Update
                                     </button>

                                   </div>

                                    {{-- <button id="search_btn" name="search_btn" class="btn btn-danger">Search coupons</button> --}}
                                 </div>
                              </fieldset>
                           </form>
                         </div>
                       </div>
                     </div>
                     </div>


                  </div>
               </div>
            </div>
         </section>
         <!-- Footer -->
           @include('backend.includes.mainfooter')
         <!-- start modal -->
       <!-- Large modal -->

         <!-- end: Modall -->
      </div>
      <!-- //wrapper -->
      <!-- jQuery  -->
      <script src="{{config('APP.URL')}}/assets/js/jquery.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/bootstrap.min.js"></script>
      <script src="{{config('APP.URL')}}/assets/js/animsition.min.js"></script>
      <script src="{{config('APP.URL')}}/owl.carousel/owl.carousel.min.js"></script>
      <!-- Kupon js -->
      <script src="{{config('APP.URL')}}/assets/js/kupon.js"></script>

      <script type="text/javascript">
      function validate(evt) {
      var theEvent = evt || window.event;
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode( key );
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
      }
      }
      </script>

   </body>

<!-- Mirrored from codenpixel.com/demo/kuponhub/results_2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Dec 2017 08:54:38 GMT -->
</html>

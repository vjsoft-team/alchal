<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
          $table->increments('id');
          $table->String('owner_name')->nullable();
          $table->String('owner_no')->nullable();
          $table->String('contact_person_name');
          $table->String('contact_person_no');
          $table->String('shop_mobile')->nullable();
          $table->String('shop_landline')->nullable();
          $table->String('address');
          $table->String('store_logo')->nullable();
          $table->string('name');
          $table->string('email')->unique();
          $table->string('password');
          $table->string('status');
          $table->rememberToken();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->String('name');
            $table->String('owner_name')->nullable();
            $table->String('owner_no')->nullable();
            $table->String('contact_person_name');
            $table->String('contact_person_no');
            $table->String('shop_mobile')->nullable();
            $table->String('shop_landline')->nullable();
            $table->String('address');
            $table->String('shop_logo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}

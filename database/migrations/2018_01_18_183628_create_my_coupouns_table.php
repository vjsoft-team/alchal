<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyCoupounsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_coupouns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('store');
            $table->integer('coupon_id');
            $table->string('coupon_code');
            $table->integer('user_id');
            $table->string('user_phone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_coupouns');
    }
}

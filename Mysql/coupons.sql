-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2018 at 01:51 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coupons`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'venkyvj', 'venkyvj@gmail.com', '$2y$10$.EGXtbyVeQE/30.NlsnqAO532nshy9T3SBZpRnKW9hzG1MGJ./cFC', 'admin', 'ZAYraTZTj6UulrJZAGGbiGBnlPH1qwYxKe5kpj7j93kz9GUZgX39PSWZg6ro', '2017-12-18 01:24:04', '2017-12-18 01:24:04'),
(3, 'alchal09', 'alchal09', '$2y$10$LX4C0eFdw94JbLd4wE52rOzCCyAeRjypI/Cix6GlzBj11fwx8GyRW', 'admin', 'NfZLa6Kan7VEL59RCNCuhDqMerjzrSVT1NA05MlafqCrO73h2G1CKcTwVknF', '2017-12-29 04:51:10', '2017-12-29 04:51:10');

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` text COLLATE utf8mb4_unicode_ci,
  `cover_image` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `logo`, `cover_image`, `title`, `description`, `position`, `created_at`, `updated_at`) VALUES
(1, 'student1_1515760195.jpg', 'download_1515760196.jpg', 'test by vJ1', 'test by vJ', 2, '2017-12-26 00:34:29', '2018-01-12 06:59:56'),
(2, 'user_1514269048.png', 'download_1514269048.jpg', 'Up To sankranthi', 'ON Foods Mega Carnival', 3, '2017-12-26 00:47:28', '2018-01-12 07:16:50'),
(3, 'IMG-20171120-WA0005_1515761358.jpg', '8135b811d036a321a7737cc15dcd4812--african-safari-african-animals_1515761358.png', 'macxcd', 'afsr srgf', 1, '2018-01-03 00:01:41', '2018-01-12 07:19:18');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `category_image`, `created_at`, `updated_at`) VALUES
(1, 'cakes1', 'ti-bag', '2017-12-23 07:56:56', '2017-12-23 08:37:02'),
(2, 'fruits', 'ti-ticket', '2017-12-23 07:57:16', '2017-12-23 07:57:16'),
(3, 'chacolates', 'ti-pulse', '2017-12-23 07:58:25', '2017-12-23 07:58:25'),
(4, 'direction', 'ti-direction-alt', '2017-12-23 08:03:03', '2017-12-23 08:03:03'),
(5, 'default', 'ti-harddrives', '2017-12-23 08:03:21', '2017-12-23 08:03:21'),
(6, 'bangles', NULL, '2018-01-12 02:07:30', '2018-01-12 02:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `shop` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coupon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `shop`, `category`, `coupon`, `title`, `description`, `discount`, `valid_from`, `valid_to`, `created_at`, `updated_at`) VALUES
(1, 'KFC', 'chacolates', 'CHRSTMS1', 'Extra 10% Off Select Luggage + Up To $150 Back In Points For Members + Free Shipping', 'Shop these Shopname deals of the day to save as much...', '12', '2017-12-24', '2018-01-01', '2017-12-18 04:32:58', '2017-12-18 04:32:58'),
(2, 'Hangouts', 'cakes1', 'CHRSTMS', 'Up To $150 Back In Points For Members + Free Shipping', 'of the day to save as much...', '13', '2017-12-27', '2018-01-03', '2017-12-18 04:33:39', '2017-12-18 04:33:39'),
(3, 'Manasa', 'direction', 'NSJDUJJ6', 'Christmas special discounts', 'On Cakes & Drinks majasf', '59', '2017-12-25', '2018-01-02', '2017-12-18 06:27:00', '2017-12-18 06:27:00'),
(4, 'keys', '', 'MASFIHsa', 'Sanranthi Offers', 'ON Foods Mega Carnival', '49', '2017-12-31', '2018-01-31', '2017-12-18 06:28:27', '2017-12-18 06:28:27'),
(5, 'KFC', 'chacolates', 'DAMAAKA', 'djsfisdafhafjasnfldfasasd', 'asfasrewgfbgfjtgjhf', '50', '2017-12-25', '2018-01-03', '2017-12-18 06:28:55', '2017-12-18 06:28:55'),
(6, 'keys', 'cakes1', 'ZXACSV', 'checkout', 'sdgs sfgds', '25', '2017-12-28', '2018-01-06', '2017-12-23 10:09:35', '2017-12-23 10:09:35');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_12_14_110616_create_admins_table', 1),
(4, '2017_12_16_065956_create_coupons_table', 1),
(5, '2017_12_18_061821_create_shops_table', 1),
(6, '2017_12_23_120553_create_categories_table', 2),
(7, '2017_12_25_162825_create_ads_table', 3),
(8, '2018_01_12_074419_create_positions_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_person_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_landline` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `name`, `owner_name`, `owner_no`, `contact_person_name`, `contact_person_no`, `shop_mobile`, `shop_landline`, `address`, `shop_logo`, `created_at`, `updated_at`) VALUES
(2, 'Hangouts', NULL, NULL, 'venky', '9324932728', NULL, NULL, 'aksfoasf.mfjof-517501', NULL, '2017-12-18 01:41:08', '2017-12-18 01:41:08'),
(3, 'KFC', 'fsdf', '8936493265', 'sdjfhos', '2938469832', '2432432434', '324324', 'nasfias-517501', NULL, '2017-12-18 01:41:42', '2017-12-18 01:41:42'),
(4, 'Manasa', 'sfn', '7828432947', 'safuiew', '2891732342', '3242342342', '2134243242', 'asfasdasdasdd', NULL, '2017-12-18 06:26:14', '2017-12-18 06:26:14'),
(5, 'keys', 'nashf', '2874832743', 'jhsjfhsie', '83748', '3287943274', '9473897', 'afhshfs', 'download_1514042501.jpg', '2017-12-18 06:27:29', '2017-12-18 06:27:29'),
(6, 'MArasa', 'adsa', '2874832743', 'sdjfhos', '2938469832', '3242342342', '324324', 'sgsg sdfsv wef', 'download_1514042501.jpg', '2017-12-23 09:04:21', '2017-12-23 09:04:21'),
(7, 'barber shop', 'vJ vENKY', '9490501349', 'sikindhar', '9177171471', '7013089044', '0877228448', '20-1-471/h5/g', 'download_1514042501.jpg', '2017-12-23 09:19:36', '2017-12-23 09:19:36'),
(8, 'test', 'asfsdf', '4324354354', 'dvs', '2132131321', '2312432424', '24324', 'casaa aara', 'download (1)_1514042421.jpg', '2017-12-23 09:50:21', '2017-12-23 09:50:21'),
(9, 'kisfsf', 'asfsaf', NULL, 'sgfgfg', '1232142112', NULL, NULL, 'asfeff', '8135b811d036a321a7737cc15dcd4812--african-safari-african-animals_1514042579.jpg', '2017-12-23 09:51:41', '2017-12-23 09:51:41'),
(10, 'as', NULL, NULL, 'sdasfaf', '2321432432', NULL, NULL, 'fsafaersaff', '8135b811d036a321a7737cc15dcd4812--african-safari-african-animals_1514042579.jpg', '2017-12-23 09:52:59', '2017-12-23 09:52:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `mobile`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mamatha', '', 'mamatha@gmail.com', '$2y$10$wno3Ic83VLrGLTGl2auJE.frEkFzmglZV1K8Kn6C0clHncCTHDF92', '58Iff9TIrRWLkPJYlZHsR8HtAFNrThqLZtISpQy9wYSgmbVSUw6V0ZZhssuA', '2017-12-25 10:01:47', '2017-12-25 10:01:47'),
(2, 'vcrnaidu', '', 'vcrnaidu@gmail.com', '$2y$10$o6aPeBgB5WclFr9E2uNIhOhVoB6XPn7g6siMRNISjOkV.dobKg/By', 'VBS74WVesCCXEvC11LA3DqlpMc1UR8tCXtAmOWUl5v7LbB9gKfWJAk1ftqzh', '2017-12-25 10:11:20', '2017-12-25 10:11:20'),
(3, 'siki', '9177171471', 'siki@gmail.com', '$2y$10$XoFdX8Yk/T4.vDG98HEtCOTS/end6sYg1iXufqPatfFnzxEToWjse', 'Qqon6y4U22JyXD4so0jdxrpxHak33SF93YYg340QYAOKUTlqLqXmxfXJ7IEZ', '2018-01-12 06:03:58', '2018-01-12 06:03:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
